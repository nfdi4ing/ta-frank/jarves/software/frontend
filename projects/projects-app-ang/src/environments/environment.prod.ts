export const environment = {
  production: true,
  frontend_url_prefix: "//jarves.nfdi4ing.de",
  backend_url_prefix: "//backend.jarves.nfdi4ing.de",
};
