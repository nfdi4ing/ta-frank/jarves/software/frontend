export interface Training {
  /* 
    In Serializer
    'url', 'id', 'title', 'link', 'language', 'phase', 'step', 'tag', 'author', 'format', 'comment', 'rating', 'voting' */

  id?: number;
  title?: string;
  fit?: number;
  link?: string;
  language?: string;
  phase?: string;
  step?: string;
  tag?: string;
  author?: string;
  format?: string;
  comment?: string;
  rating?: string;
}
