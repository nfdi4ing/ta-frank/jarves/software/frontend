export interface Policy {
    id?: number;
    name?: string;
    kind?: PolicyKind;
    dmp_recommendation?: DmpTool;
    storage_recommendation?: StorageTool;
    score1?: number;
    score2?: number;
}

export interface PolicyKind {
    id?: number;
    name?: string;
}

export interface DmpTool {
    id?: number;
    name?: string;
    url?: string;
}

export interface StorageTool {
    id?: number;
    name?: string;
    url?: string;
}