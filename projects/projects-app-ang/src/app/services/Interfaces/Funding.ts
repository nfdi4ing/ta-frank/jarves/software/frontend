export interface Funding {
    id?: number;
    name?: string;
    acronym?: string;
}