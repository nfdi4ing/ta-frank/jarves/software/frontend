export interface Institute {
    id?: number;
    name?: string;
    description?: string;
    universityId?: number;
}