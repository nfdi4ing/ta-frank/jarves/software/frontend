export interface University {
    id?: number;
    name?: string;
    shortTitle?: string;
    displayName?: string;
    description?: string;
    acronym?: string;
}