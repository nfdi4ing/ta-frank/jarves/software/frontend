import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {
  HttpClient,
  HttpParams,
  HttpResponse,
} from '@angular/common/http'; /** Add the HTTP Client for HTTP requests */

import { Project } from '../Project';
import { MessageService } from '../message.service';
import { environment } from '@env';

/** See also: https://angular.io/guide/http */

@Injectable({
  providedIn: 'root',
})
export class ProjectAppService {
  private baseUrl = `${environment.backend_url_prefix}/ProjectAPP/ProjectViewSet/`;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) {} /** Construct the HTTP Client */

  getAll(): Observable<Project[]> {
    /** Request a GET : Observable<Project[]> */
    return (
      this.http
        .get<Project[]>(this.baseUrl, {
          observe: 'body',
          withCredentials: true,
        })
        /** Catch and handle errors here while requesting */
        .pipe(
          tap(() => this.log('fetched projects')),
          catchError(this.handleError<Project[]>('getAll', [])),
          //map((res: any) => {
          //return res.projects.map((e: { title: string; short_title: string; }) => {
          //return { title: e.title, short_title: e.short_title };
          //})
        )
    );
  }

  get(id: string): Observable<Project> {
    return this.http.get(`${this.baseUrl}${id}/`, { withCredentials: true });
  }
  create(data: any): Observable<any> {
    console.log('Submitted Project: In projectapp service');
    return this.http.post(this.baseUrl, data, { withCredentials: true });
  }
  update(id: any, data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}${id}/`, data, {
      withCredentials: true,
    });
  }
  delete(id: any): Observable<any> {
    return this.http.delete(`${this.baseUrl}${id}/`, { withCredentials: true });
  }
  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl, { withCredentials: true });
  }

  findByTitle(title: any): Observable<Project[]> {
    if (!title.trim()) {
      // If no project is found, that contains (trim) this title string, an empty array is returned
      return of([]);
    }
    return this.http
      .get<Project[]>(`${this.baseUrl}?title=${title}`, {
        withCredentials: true,
      })
      .pipe(
        tap((x) =>
          x.length
            ? this.log(`found projects matching "${title}"`)
            : this.log(`no projects matching "${title}"`),
        ),
        catchError(this.handleError<Project[]>('searchProjects', [])),
      );
    // return this.http.get<Project[]>(`${this.baseUrl}?title=${title}`);
    // return this.http
  }

  //send user to be added to project
  post_userid(id: any, userid: any): Observable<any> { 
    console.log(id + "##" +userid);
    return this.http.post(
      `${this.baseUrl}${id}/adduser`,
      JSON.stringify({ user : userid }),
      { withCredentials: true, headers: { 'Content-Type': 'application/json' }},
    );
  }
  post_policy(id: any, university: any, institute: any, funding: any): Observable<any> {
    return this.http.post(
      `${this.baseUrl}${id}/addpolicy`,
      JSON.stringify({ university: university, institute: institute, funding: funding}),
      { withCredentials: true, headers: { 'Content-Type': 'application/json' }},
    );
  }


  findProjects(
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 5,
    attribute = 'title',
  ): Observable<HttpResponse<Project[]>> {
    return this.http
      .get<Project[]>(this.baseUrl, {
        withCredentials: true,
        observe: 'response',
        params: new HttpParams()
          .set('filter', filter)
          .set('sortOrder', sortOrder)
          .set('pageNumber', pageNumber.toString())
          .set('pageSize', pageSize.toString())
          .set('attribute', attribute),
      })
      .pipe(tap(() => this.log(`Passing HTTP Response to component`)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    /**
      Handle Http operation that failed.
      Let the app continue.
      @param operation - name of the operation that failed
      @param result - optional value to return as the observable result
     */
    return (error: { message: string }): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  assignRole(data: any): Observable<any> {

    console.log('Request')
    return this.http.post(this.baseUrl, data, { withCredentials: true });
      
  }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }
}
