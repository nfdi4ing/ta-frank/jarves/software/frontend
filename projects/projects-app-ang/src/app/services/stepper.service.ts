import { Injectable, ViewChild } from '@angular/core';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StepperService {

  private mainStepper: NbStepperComponent;
  private currentStep: BehaviorSubject<number> = new BehaviorSubject(-1);

  setMainStepper(stepper: NbStepperComponent) {
    this.mainStepper = stepper;
    // console.log('MainStepper received:', this.mainStepper);
    this.mainStepper.stepChange.subscribe(() => {
      this.currentStep.next(this.mainStepper.selectedIndex);
      // console.log('Current step:', this.mainStepper.selectedIndex);
    });
  }

  getCurrentStep(): Observable<number> {
    return this.currentStep.asObservable();
  }

  /* private event$: BehaviorSubject<
    keyof typeof EVENT | null
  > = new BehaviorSubject(null as keyof typeof EVENT | null); */

  

  @ViewChild('stepperMain') stepper8: NbStepperComponent;
  @ViewChild('stepperEinleitung') stepper1: NbStepperComponent;
  @ViewChild('stepperProjektuebersicht') stepper2: NbStepperComponent;
  @ViewChild('stepperErhebung') stepper4: NbStepperComponent;
  @ViewChild('stepperAnalyse') stepper5: NbStepperComponent;
  @ViewChild('stepperArchivieren') stepper6: NbStepperComponent;
  @ViewChild('stepperAccess') stepper7: NbStepperComponent;

  

  // Array of completion statuses for each step
  stepsCompleted: boolean[] = [false, false, false];

  // Index of currently active step
  activeStepIndex = 0;
  stepper: any;

  constructor() { }



  // Mark current step as completed
  markCurrentStepComplete(stepper: NbStepperComponent) {
    this.stepsCompleted[this.activeStepIndex] = true;
    stepper.next();
  }

  // Move to previous step in stepper
  goToPreviousStep(stepper: NbStepperComponent) {
    stepper.previous();
  }

  // Move to next step in stepper
  goToNextStep(stepper: NbStepperComponent, step: NbStepComponent) {
    if (step.completed) {
      stepper.next();
    }
  }

  completedSteps = [false, false, false];

  isStepCompleted(stepNumber: number, stepper: NbStepperComponent): boolean {
    if (stepper && stepper.steps) {
      const stepsArray = stepper.steps.toArray();
      if (stepNumber < stepsArray.length) {
        return stepsArray[stepNumber].completed;
      }
    }
    return false;
  }

  resetStep(index: number, stepper?: NbStepperComponent): () => void {
    if (stepper) {
      return () => {
        stepper.steps.toArray()[index].completed = false;
      };
    } else {
      return () => {
        console.log(`Step ${index + 1} reset`);
      };
    }
  }

  completeStep(stepNumber: number, stepper: NbStepperComponent): void {
    stepper.steps.toArray()[stepNumber].completed = true;
    stepper.next();
  }
}
