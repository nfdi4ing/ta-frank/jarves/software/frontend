import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http'; /** Add the HTTP Client for HTTP requests */

import { MessageService } from '../message.service';
import { environment } from '@env';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  private baseUrl = `${environment.backend_url_prefix}/Feedback/NeedForHelpViewSet/`;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) {} /** Construct the HTTP Client */

  create(): Observable<any> {
    console.log('Create');

    console.log(this.http.post(this.baseUrl, null));

    return this.http.post(this.baseUrl, null);
  }

  getNeedsForHelp(): Observable<any> {
    return this.http.get(`${this.baseUrl}?Latest=True`);
  }

  // Methode, die den E-Mail-Client öffnet und die E-Mail generiert
  openEmailClient(): void {
    const emailSubject = `[NFDI4Ing] Jarves: `;
    const emailBody = `Beschreibung: Problem in: ${this.currentStep}`;
    const emailLink = `mailto:jarves@nfdi4ing.tu-darmstadt.de?subject=${encodeURIComponent(
      emailSubject,
    )}&body=${encodeURIComponent(emailBody)}`;
    window.location.href = emailLink; // Öffnet den E-Mail-Client
  }

  // Feedback-Variable
  currentStep = ''; // Aktueller Schritt

  // Methode, die aufgerufen wird, wenn der Button "Feedback geben" geklickt wird
  giveFeedback(stepName: string): void {
    this.currentStep = stepName;
    // Hier können Sie den Code für das Öffnen des E-Mail-Clients hinzufügen
    this.openEmailClient();
  }
}
