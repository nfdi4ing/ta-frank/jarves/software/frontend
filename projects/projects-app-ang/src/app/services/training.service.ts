import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {
  HttpClient,
  HttpParams,
  HttpResponse,
} from '@angular/common/http'; /** Add the HTTP Client for HTTP requests */

import { Training } from './Interfaces/Training';
import { MessageService } from '../message.service';
import { environment } from '@env';

@Injectable({
  providedIn: 'root',
})
export class TrainingService {
  private baseUrl = `${environment.backend_url_prefix}/trainings/TrainingViewSet/`;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) {}

  getAll(): Observable<Training[]> {
    /** Request a GET : Observable<Project[]> */
    return (
      this.http
        .get<Training[]>(this.baseUrl, {
          observe: 'body',
          withCredentials: true,
        })
        /** Catch and handle errors here while requesting */
        .pipe(
          tap(() => this.log('fetched projects')),
          catchError(this.handleError<Training[]>('getAll', [])),
          //map((res: any) => {
          //return res.projects.map((e: { title: string; short_title: string; }) => {
          //return { title: e.title, short_title: e.short_title };
          //})
        )
    );
  }

  findTrainings(
    /* 
    In Serializer
    'url', 'id', 'title', 'link', 'language', 'phase', 'step', 'tag', 'author', 'format', 'comment', 'rating' */

    title = '',
    project = '',
    fit = 100,
    sortOrder = 'asc',
    sortAttribute = '',
    pageIndex = 0,
    pageSize = 5,
    language = '',
    phase = '',
    step = '',
    tag = '',
    author = '',
    format = '',
    comment = '',
    rating = '',
  ): Observable<HttpResponse<Training[]>> {
    console.log(`${this.baseUrl}`);
    return this.http
      .get<Training[]>(this.baseUrl, {
        withCredentials: true,
        observe: 'response',
        params: new HttpParams()
          .set('title', title)
          .set('project', project)
          .set('fit', fit)
          .set('sortOrder', sortOrder)
          .set('sortAttribute', sortAttribute)
          .set('pageIndex', pageIndex)
          .set('pageSize', pageSize)
          .set('language', language)
          .set('phase', phase)
          .set('step', step)
          .set('tag', tag)
          .set('author', author)
          .set('format', format)
          .set('comment', comment)
          .set('rating', rating),
      })
      .pipe(tap(() => this.log(`Passing Training HTTP Response to component`)));
  }

  voteTraining(TrainingID: string, Rating: number): Observable<any> {
    return this.http.patch(
      `${this.baseUrl}${TrainingID}/`,
      { rating: Rating },
      {
        withCredentials: true,
      },
    );
  }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    /**
      Handle Http operation that failed.
      Let the app continue.
      @param operation - name of the operation that failed
      @param result - optional value to return as the observable result
     */
    return (error: { message: string }): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
