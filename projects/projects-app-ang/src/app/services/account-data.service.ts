import { Injectable } from '@angular/core';

import { Observable, map } from 'rxjs';

import { HttpClient } from '@angular/common/http';

import { environment } from '@env';

import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AccountDataService {
  private baseUrl = `${environment.backend_url_prefix}/apiservices`;
  private darkModeEnabled: boolean = false;
  private URLTextCom: string;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    // Initialize URLTextCom in the constructor
    this.updateURLTextCom();
  }

  // Update URLTextCom based on the last number from the current route
  private updateURLTextCom(): void {
    // Get the 'id' parameter from the current route
    const projectId = this.getCurrentProjectId();

    // Update URLTextCom based on the projectId
    this.URLTextCom = `${environment.backend_url_prefix}/ProjectAPP/user_text_communication/${projectId}/`;
  }

  private getCurrentProjectId(): string {
    // Get the current URL
    const currentUrl = window.location.href;

    // Extract the last part of the URL as the project ID
    const parts = currentUrl.split('/');
    return parts[parts.length - 1];
  }

  // GET request to get data from user communication fields
  getUserTextCommunication(): Observable<any> {
    return this.http.get(this.URLTextCom, { withCredentials: true });
  }

  // POST request to save data from user communication fields
  saveUserTextCommunication(data: any): Observable<any> {
    const headers = {
      'Content-Type': 'application/json',
    };

    return this.http.post(this.URLTextCom, data, { withCredentials: true, headers });
  }

  get_darkmode(): Observable<boolean> {
    return this.http
      .get<{ darkmode: boolean }>(`${this.baseUrl}/darkmode/`, {
        withCredentials: true,
      })
      .pipe(map((r) => r.darkmode));
  }

  post_darkmode(value: boolean): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/darkmode/`,
      JSON.stringify({ darkmode: value }),
      {
        withCredentials: true,
        headers: { 'Content-Type': 'application/json' },
      },
    );
  }

  isDarkModeEnabled(): boolean {
    return this.darkModeEnabled;
  }

  setDarkModeEnabled(enabled: boolean): void {
    this.darkModeEnabled = enabled;
  }

  get_apikey(): Observable<string> {
    return this.http
      .get<{ key: string }>(`${this.baseUrl}/api-key/`, {
        withCredentials: true,
      })
      .pipe(map((r) => r.key));
  }

  post_apikey(apikey: string): Observable<any> {
    // Retrieve CSRF token from the cookie
    const csrfToken = this.getCookie('csrftoken');

    // Include CSRF token in the headers
    const headers = {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrfToken,
    };

    console.log('Request Headers:', headers); // Log headers for troubleshooting

    return this.http.post(
      `${this.baseUrl}/api-key/`,
      JSON.stringify({ key: apikey }),
      {
        withCredentials: true,
        headers: headers,
      }
    );
  }

  // Helper function to get a cookie value by name
  private getCookie(name: string): string {
    const match = document.cookie.match(new RegExp(name + '=([^;]+)'));
    return match ? match[1] : '';
  }

  get_email(): Observable<string> {
    return this.http
      .get<{ key: string }>(`${this.baseUrl}/email/`, { withCredentials: true })
      .pipe(map((r) => r.key));
  }

  post_email(email: string): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/email/`,
      JSON.stringify({ key: email }),
      {
        withCredentials: true,
        headers: { 'Content-Type': 'application/json' },
      },
    );
  }

  get_apikey_coscine(): Observable<string> {
    return this.http
      .get<{ key: string }>(`${this.baseUrl}/api-key-coscine/`, {
        withCredentials: true,
      })
      .pipe(map((r) => r.key));
  }

  get_user_id(): Observable<string> {
    return this.http
      .get<{ key: string }>(`${this.baseUrl}/user-id/`, {
        withCredentials: true,
      })
      .pipe(map((r) => r.key));
  }

  post_apikey_coscine(apikey: string): Observable<any> {
    return this.http.post(
      `${environment.backend_url_prefix}/apiservices/api-key-coscine/`,
      JSON.stringify({ key: apikey }),
      {
        withCredentials: true,
        headers: { 'Content-Type': 'application/json' },
      },
    );
  }

  get_all_users(): Observable<any> {
    return this.http.get<{ users: any }>(
      `${this.baseUrl}/get-all-users/`,
      { withCredentials: true },
    );
  }

  /*get_all_users(): Observable<any> {
    const url = `${this.baseUrl}/get-all-users/`;
    return this.http.get<any>(url);
  } */

  get_logged_in_user(): Observable<{ username: string; email: string }> {
    return this.http.get<{ username: string; email: string }>(
      `${this.baseUrl}/get-logged-in-user/`,
      { withCredentials: true },
    );
  }
}


