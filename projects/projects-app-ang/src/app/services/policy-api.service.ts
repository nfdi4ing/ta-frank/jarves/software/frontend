import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {
  HttpClient,
  HttpParams,
  HttpResponse,
} from '@angular/common/http'; /** Add the HTTP Client for HTTP requests */

import { Project } from '../Project';
import { MessageService } from '../message.service';
import { environment } from '@env';
import { University } from './Interfaces/University';
import { Policy, PolicyKind } from './Interfaces/Policy';
import { Funding } from './Interfaces/Funding';

/** See also: https://angular.io/guide/http */

@Injectable({
  providedIn: 'root',
})
export class PolicyApiService {
  private baseUrl = `${environment.backend_url_prefix}/Policies/`;
  constructor(private http: HttpClient) {}

  getAllUniversities(): Observable<any> {
    return this.http.get(this.baseUrl + 'universities/', {
      withCredentials: true,
    });
  }
  getUniversity(id: number): Observable<any> {
    return this.http.get(this.baseUrl + 'universities/' + id + '/', {
      withCredentials: true,
    });
  }

  createUniversity(university: University): Observable<any> {
    return this.http.post(this.baseUrl + 'universities/create/', university, {
      withCredentials: true,
    });
  }

  getAllInstitutes(): Observable<any> {
    return this.http.get(this.baseUrl + 'institutes/', {
      withCredentials: true,
    });
  }
  getInstitute(id: number): Observable<any> {
    return this.http.get(this.baseUrl + 'institutes/' + id + '/', {
      withCredentials: true,
    });
  }
  createInstitute(institute: any): Observable<any> {
    return this.http.post(this.baseUrl + 'institutes/create/', institute, {
      withCredentials: true,
    });
  }

  getAllPolicies(): Observable<Policy[]> {
    return this.http.get<Policy[]>(this.baseUrl + 'policies/', {
      withCredentials: true,
    });
  }

  createPolicy(policy: Policy): Observable<any> {
    return this.http.post(this.baseUrl + 'policies/create/', policy, {
      withCredentials: true,
    });
  }

  getAllPolicyKinds(): Observable<PolicyKind[]> {
    return this.http.get<PolicyKind[]>(this.baseUrl + 'policy-kinds/', {
      withCredentials: true,
    });
  }
  getAllDmpTools(): Observable<any> {
    return this.http.get(this.baseUrl + 'dmp_tools/', {
      withCredentials: true,
    });
  }
  getAllStorageTools(): Observable<any> {
    return this.http.get(this.baseUrl + 'storage-tools/', {
      withCredentials: true,
    });
  }

  getProjectPolicies(projectId: string): Observable<any> {
    return this.http.get(this.baseUrl + 'project-policies/' + projectId + '/', {
      withCredentials: true,
    });
  }

  getAllFundings(): Observable<any> {
    return this.http.get(this.baseUrl + 'funding/', {
      withCredentials: true,
    });
  }
  createFunding(funding: Funding): Observable<any> {
    return this.http.post(this.baseUrl + 'funding/create/', funding, {
      withCredentials: true,
    });
  }

}
