import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http'; /** Add the HTTP Client for HTTP requests */

import { Account } from '../Account';
import { MessageService } from '../message.service';
import { environment } from '@env';

@Injectable({
  providedIn: 'root',
})
export class AccountDataService {
  private baseUrl = `${environment.backend_url_prefix}/ProjectAPP/ProjectViewSet/`;
  //private baseUrl = 'https://127.0.0.1:8000/ProjectAPP/ProjectViewSet/'; /** Add the baseUrl for the App */
  // private baseUrl = "http://127.0.0.1:8000/ProjectAPP/ProjectViewSet/";

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) {} /** Construct the HTTP Client */

  getDarkMode(): Observable<boolean> {
    console.log('getDarkMode()'); // log to console
  }

  getAll(): Observable<Account> {
    /** Request a GET : Observable<Account> */
    return (
      this.http
        .get<Project[]>(this.baseUrl, {
          observe: 'body',
          withCredentials: true,
        })
        /** Catch and handle errors here while requesting */
        .pipe(
          tap(() => this.log('fetched projects')),
          catchError(this.handleError<Project[]>('getAll', [])),
          //
          //map((res: any) => {
          //return res.projects.map((e: { title: string; short_title: string; }) => {
          //return { title: e.title, short_title: e.short_title };
          //})
        )
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    /**
      Handle Http operation that failed.
      Let the app continue.
      @param operation - name of the operation that failed
      @param result - optional value to return as the observable result
     */
    return (error: { message: string }): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }
}
