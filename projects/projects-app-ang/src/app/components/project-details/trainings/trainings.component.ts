import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize, tap, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { environment } from '@env';
import { ActivatedRoute } from '@angular/router';
import { Input, ChangeDetectorRef } from '@angular/core';

/* Custom Imports */
import { Training } from '@services/Interfaces/Training';
import { TrainingService } from '@services/training.service';
import { MessageService } from '../../../message.service';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.css'],
})
export class TrainingsComponent implements OnInit, AfterViewInit {
  @Input() currentStep: [string, string]; // Eingabe für den aktuellen Schritt und den "Stepper"

  training!: Training[];
  dataSource!: TrainingDataSource;
  TrainingCount!: any;
  frontend_url: string;
  displayedColumns = ['title', 'link', 'rating'];
  project!: string;
  activePhaseIndex: string;
  activeStepIndex: string;

  constructor(
    private trainingService: TrainingService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.frontend_url = environment.frontend_url_prefix;
    this.project =
      this.route.snapshot.params[
        'id'
      ]; /*this.route.url;  need to implement to figure out current project this.getProject(this.route.snapshot.params["id"]); */
    this.dataSource = new TrainingDataSource(
      this.trainingService,
      this.messageService,
    );
    this.updateActivePhase();
    this.updateActiveStep();
    this.getTrainings(this.dataSource);
    this.TrainingCount = this.dataSource.TrainingCount;
  }

  ngAfterViewInit(): void {
    // server-side search
    console.log('Training ngAfterViewInit');
  }

  updateActivePhase() {
    // Extrahiere die Phase (eine Zeichenkette) aus currentStep
    const phase: string = this.currentStep[1];
    // Weise die Phase der activePhaseIndex-Eigenschaft zu
    this.activePhaseIndex = phase;
  }

  updateActiveStep() {
    // Extrahiere den Schritt (eine Zahl) aus currentStep
    const step: string = this.currentStep[0];
    // Weise den Schritt der activeStepIndex-Eigenschaft zu
    this.activeStepIndex = step;
  }

  getTrainings(datasource: TrainingDataSource) {
    datasource.loadTrainings(
      '' /* Title */,
      this.project /* Give the current project to the backend */,
      100 /* Fit-Threshold */,
      'desc' /* Sorting Order */,
      'rating' /* Sorting Attribute */,
      0 /* Page Index */,
      5 /* Page Size */,
      '' /* Language */,
      this
        .activePhaseIndex /* Phase --> This should be the main filter for trainings*/,
      this
        .activeStepIndex /* Step --> This should be the main filter for trainings*/,
      '' /* tag  --> This should be the main filter for trainings*/,
      '' /* author */,
      '' /* format */,
      '' /* comment */,
      '' /* rating-Threshold */,
    );
  }

  upvoteTraining(
    TrainingID: string,
    datasource: TrainingDataSource,
    CurrentRating: number,
  ) {
    datasource.voteTrainings(TrainingID, CurrentRating + 1);
    setTimeout(() => {
      this.getTrainings(this.dataSource);
      this.cdr.detectChanges();
    }, 500); // 500 milliseconds (0.5 seconds)
  }

  downvoteTraining(
    TrainingID: string,
    datasource: TrainingDataSource,
    CurrentRating: number,
  ) {
    datasource.voteTrainings(TrainingID, CurrentRating - 1);
    setTimeout(() => {
      this.getTrainings(this.dataSource);
      this.cdr.detectChanges();
    }, 500); // 500 milliseconds (0.5 seconds)
  }
}

export class TrainingDataSource implements DataSource<Training> {
  private trainingsSubject = new BehaviorSubject<Training[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private trainingCountSubject = new BehaviorSubject<string>('');

  public loading$ = this.loadingSubject.asObservable();
  public TrainingCount = this.trainingCountSubject.asObservable();

  trainings: Training[] = [];

  constructor(
    private trainingService: TrainingService,
    private messageService: MessageService,
  ) {}

  connect(): Observable<Training[]> {
    return this.trainingsSubject.asObservable();
  }

  // rm collectionViewer: CollectionViewer
  disconnect(): void {
    this.trainingsSubject.complete();
    this.loadingSubject.complete();
    this.trainingCountSubject.complete();
  }

  loadTrainings(
    title = '',
    project = '',
    fit: number,
    sortOrder = 'asc',
    sortAttribute: string,
    pageIndex: number,
    pageSize: number,
    language: string,
    phase: string,
    step: string,
    tag: string,
    author: string,
    format: string,
    comment: string,
    rating: string,
  ) {
    this.loadingSubject.next(true);
    console.log('Phase');
    console.log(phase);
    console.log('Step');
    console.log(step);

    this.trainingService
      .findTrainings(
        title,
        project,
        fit,
        sortOrder,
        sortAttribute,
        pageIndex,
        pageSize,
        language,
        phase,
        step,
        tag,
        author,
        format,
        comment,
        rating,
      )
      .pipe(
        map((response: HttpResponse<Training[]>) => {
          const trainings: Training[] = response.body || [];
          const trainingCount = response.headers.get('trainingcount') || '';
          this.trainingsSubject.next(trainings);
          this.trainingCountSubject.next(trainingCount);
          return trainings;
        }),
        finalize(() => this.loadingSubject.next(false)),
        tap(() => this.log(`Load Trainings`)),
      )
      .subscribe();
  }

  getAllTrainings(): void {
    this.trainingService
      .getAll()
      .subscribe((training) => this.trainingsSubject.next(training));
  }

  voteTrainings(TrainingID: string, Rating: number): void {
    this.trainingService
      .voteTraining(TrainingID, Rating)
      .subscribe((training) => this.trainingsSubject.next(training));
  }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }
}
