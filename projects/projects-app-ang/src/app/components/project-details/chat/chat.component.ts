import { Component, Input, OnInit } from '@angular/core';
import { AccountDataService } from '@services/account-data.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit {
  @Input('i') index: any; // Input parameter with the name 'i'
  textFields: any;
  textField: any;
  textFieldsObject: any;

  constructor(private account: AccountDataService) { }

  ngOnInit(): void {
    // GET-Anfrage, um Daten zu laden
    this.account.getUserTextCommunication().subscribe((data: any) => { 
      this.textFieldsObject = data;
      //this.textFields = Object.values(this.textFieldsObject);
      this.textField = this.textFieldsObject[this.index];

    });
  }

  onSaveClick() {
    // Update the specific field in textFieldsObject based on the index
    this.textFieldsObject[this.index] = this.textField;
    console.log(this.textFieldsObject)
    // Save the updated data
    this.account.saveUserTextCommunication(this.textFieldsObject).subscribe(
      (response: any) => { 
        console.log('Data saved successfully:', response);
      },
      (error: any) => {
        console.error('Error saving data:', error);
      }
    );
  }
}
