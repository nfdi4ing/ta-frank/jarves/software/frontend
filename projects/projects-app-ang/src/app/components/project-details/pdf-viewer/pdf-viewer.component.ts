// pdf-viewer.component.ts

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pdf-viewer',
  template: `<iframe [src]="pdfUrl" width="100%" height="500px"></iframe>`,
})
export class PdfViewerComponent {
  @Input() pdfUrl: string;
}
