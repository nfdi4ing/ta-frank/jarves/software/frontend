import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css'],
})
export class InviteComponent implements OnInit {
  constructor() {
    console.log('constructor() invite');
  }

  textInput: string;

  invite() {
    console.log(this.textInput);
  }

  ngOnInit(): void {
    console.log('ngOnInit() invite');
  }
}
