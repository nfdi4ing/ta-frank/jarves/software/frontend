// datei-upload.component.ts

import { Component } from '@angular/core';

@Component({
  selector: 'app-datei-upload',
  templateUrl: './datei-upload.component.html',
})
export class DateiUploadComponent {
  selectedFile: File | null = null;

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    if (this.selectedFile) {
      // Hier können Sie den ausgewählten Dateiinhalt an Ihren Server senden, um ihn zu speichern.
      // Verwenden Sie dafür z.B. den Angular HTTP-Client.
    }
  }
}
