import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateiUploadComponent } from './datei-upload.component';

describe('DateiUploadComponent', () => {
  let component: DateiUploadComponent;
  let fixture: ComponentFixture<DateiUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DateiUploadComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DateiUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
