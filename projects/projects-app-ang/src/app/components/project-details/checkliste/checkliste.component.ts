import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkliste',
  templateUrl: './checkliste.component.html',
  styleUrls: ['./checkliste.component.css'],
})
export class ChecklisteComponent implements OnInit {
  constructor() {
    this.checklistItems = [
      'Rollen verteilen',
      'DMP Software auswählen',
      'Fragenkatalog auswählen',
      'Ontology auswählen',
      'Urheberrechtliche Fragestellungen berücksichtigen',
      'Antrag stellen',
      'Projekt umsetzen',
      'Speichermedium auswählen',
      'Speicherformat auswählen',
    ];
  }

  ngOnInit(): void {
    console.log('ngOnInit() checklist');
  }

  checklistItems: any[] = [];
}
