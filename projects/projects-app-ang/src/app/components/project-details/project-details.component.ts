import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from '@env';
import { AccountDataService } from '@services/account-data.service';
import { Project } from '../../Project';
import { ProjectAppService } from '../../services/project-app.service';
import { TranslateService } from '@ngx-translate/core';
import { StepperService } from '@services/stepper.service';
import { FeedbackService } from '@services/feedback.service';
import { HostListener } from '@angular/core';


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
})
export class ProjectDetailsComponent implements OnInit {
  @ViewChild('stepperMain') mainStepper: NbStepperComponent;

  @HostListener('window:keydown', ['$event'])

  handleKeyboardEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowUp':
        event.preventDefault();
        this.mainStepper.previous();
        break;
      case 'ArrowDown':
        event.preventDefault();
        this.mainStepper.next();
        break;
    }
  }

  @Input() viewMode = false;
  @Input() currentProject: Project = {
    title: '',
    description: '',
  };
  message = '';
  nbPopoverText: string;
  allUsers: any; //Account [];
  user: string;
  


  

  @ViewChild('stepperMain') stepper8: NbStepperComponent;
  @ViewChild('stepperEinleitung') stepper1: NbStepperComponent;
  @ViewChild('stepperProjektuebersicht') stepper2: NbStepperComponent;
  @ViewChild('stepperExecution') stepper3: NbStepperComponent;
  @ViewChild('stepperArchivieren') stepper6: NbStepperComponent;
  @ViewChild('stepperAccess') stepper7: NbStepperComponent;

  constructor(
    private projectService: ProjectAppService,
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private account: AccountDataService,
    private translate: TranslateService,
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
  ) {}

  ngOnInit() {
    if (!this.viewMode) {
      this.message = '';
      this.getProject(this.route.snapshot.params['id']);
      this.getPdfFileUrl();
      this.getUsers();
    }
  }

  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.mainStepper);
  }

  pdfFileUrl = '';

  getPdfFileUrl() {
    // Beispiel: Pfad zur PDF-Datei von Server
    this.pdfFileUrl =
      'https://zenodo.org/records/4015201/files/nfdi4ing_proposal.pdf';
  }


  
  getProject(id: string): void {
    this.projectService.get(id).subscribe({
      next: (data) => {
        this.currentProject = data;
      },
      error: (e) => console.error(e),
    });
  } 
  

  

  getUsers(): void {
    this.account.get_all_users().subscribe({
      next: (data: any) => { 
        this.allUsers = data.users;
        console.log(data);
      },
      error: (e: any) => console.error(e)
    });
  }

  userid: number;
  response_updateProjectUsers: any; // Define the responseData property
  updateProjectUsers(userid: any): void {
    console.log("inside updateprojectusers");
    // Assuming you have a currentProject property to represent the project
    this.projectService.post_userid(this.currentProject.id, userid).subscribe(
      (response: any) => {
        console.log('API key submitted successfully:', response);
        this.response_updateProjectUsers = 'API key submitted successfully!';
      },
      (error) => console.error('Error:', error),
    );}
    
  
  updateStatus(status: boolean): void {
    const data = {
      title: this.currentProject.title,
      description: this.currentProject.description,
    };
    this.message = '';
    this.projectService.update(this.currentProject.id, data).subscribe({
      next: (res) => {
        console.log(res);
        console.log(status); // log to console
        this.message = res.message
          ? res.message
          : 'The status was updated successfully!';
      },
      error: (e) => console.error(e),
    });
  }

  updateProject(): void {
    this.message = '';
    this.projectService
      .update(this.currentProject.id, this.currentProject)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message
            ? res.message
            : 'This project was updated successfully!';
        },
        error: (e) => console.error(e),
      });
  }

  deleteProject(): void {
    this.projectService.delete(this.currentProject.id).subscribe({
      next: (res) => {
        console.log(res);
        this.router.navigate(['/projects']);
      },
      error: (e) => console.error(e),
    });
  }


}
