import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table1',
  templateUrl: './table1.component.html',
  styleUrls: ['./table1.component.css'],
})
export class Table1Component implements OnInit {
  constructor() {
    console.log('Table1 Constructor'); // log to console
  }

  ngOnInit(): void {
    console.log('ngOnInit table1'); // log to console
  }

  tableData: any[] = [
    { column1: '', column2: '', column3: '', column4: '' },
    { column1: '', column2: '', column3: '', column4: '' },
    { column1: '', column2: '', column3: '', column4: '' },
    { column1: '', column2: '', column3: '', column4: '' },
  ];
}
