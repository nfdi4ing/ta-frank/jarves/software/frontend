import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Vorbereitung2Component } from './vorbereitung2.component';

describe('Vorbereitung2Component', () => {
  let component: Vorbereitung2Component;
  let fixture: ComponentFixture<Vorbereitung2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Vorbereitung2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Vorbereitung2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
