import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Fremddaten2Component } from './fremddaten2.component';

describe('Fremddaten2Component', () => {
  let component: Fremddaten2Component;
  let fixture: ComponentFixture<Fremddaten2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Fremddaten2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Fremddaten2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
