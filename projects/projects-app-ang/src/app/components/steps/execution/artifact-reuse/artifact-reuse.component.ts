import { CommonModule } from "@angular/common";
import { Component } from '@angular/core';

@Component({
  selector: 'app-artifact-reuse',
  templateUrl: './artifact-reuse.component.html',
  styleUrls: ['./artifact-reuse.component.css'],
})
export class ArtifactReuseComponent { }
