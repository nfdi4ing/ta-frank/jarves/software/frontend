import { CommonModule } from "@angular/common";
import { Component } from '@angular/core';

@Component({
  selector: 'app-artifact-store',
  templateUrl: './artifact-store.component.html',
  styleUrls: ['./artifact-store.component.css'],
})
export class ArtifactStoreComponent { }
