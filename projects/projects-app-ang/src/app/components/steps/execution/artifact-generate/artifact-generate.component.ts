import { CommonModule } from "@angular/common";
import { Component } from '@angular/core';

@Component({
  selector: 'app-artifact-generate',
  templateUrl: './artifact-generate.component.html',
  styleUrls: ['./artifact-generate.component.css'],
})
export class ArtifactGenerateComponent { }
