import { CommonModule } from "@angular/common";
import { Component } from '@angular/core';

@Component({
  selector: 'app-artifact-check',
  templateUrl: './artifact-check.component.html',
  styleUrls: ['./artifact-check.component.css'],
})
export class ArtifactCheckComponent { }
