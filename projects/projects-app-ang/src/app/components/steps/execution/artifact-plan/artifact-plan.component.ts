import { CommonModule } from "@angular/common";
import { Component } from '@angular/core';

@Component({
  selector: 'app-artifact-plan',
  templateUrl: './artifact-plan.component.html',
  styleUrls: ['./artifact-plan.component.css'],
})
export class ArtifactPlanComponent { }
