import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DmpSoftwareComponent } from './dmp-software.component';

describe('DmpSoftwareComponent', () => {
  let component: DmpSoftwareComponent;
  let fixture: ComponentFixture<DmpSoftwareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DmpSoftwareComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DmpSoftwareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
