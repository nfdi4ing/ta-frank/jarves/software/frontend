import { Component, OnInit, Input} from '@angular/core';
import { AccountDataService } from '@services/account-data.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { Project } from '../../../Project';
import { ProjectAppService } from '../../../services/project-app.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dmp-software',
  templateUrl: './dmp-software.component.html',
  styleUrls: ['./dmp-software.component.css']
})
export class DmpSoftwareComponent implements OnInit {

  @Input() currentProject: Project = {
    title: '',
    description: '',
  };
  projectId: string;

constructor(
  private account: AccountDataService,
  private http: HttpClient,
  private projectService: ProjectAppService,
  private route: ActivatedRoute,
  private router: Router,
) { }

ngOnInit(): void {
  this.projectId = this.route.snapshot.params['id'];
  this.getProject(this.projectId);
}

title : any
description: any

getProject(id: string): void {
  this.projectService.get(id).subscribe({
    next: (data) => {
      this.currentProject = data;
      this.title = this.currentProject.title
      this.description = this.currentProject.description
    }
  });
} 



textInput: string;

responseData2: any; // Define the responseData property

submitAPIkey() {
  console.log('Submitting request...');

  this.account.post_apikey(this.textInput).subscribe(
    (response: any) => {
      console.log('RDMO API key submitted successfully:', response);
      this.responseData2 = 'RDMO API key submitted successfully!';
    },
    (error) => console.error('Error:', error),
  );
}

//api requests to coscine
responseData3: any; // Define the responseData property

performRequestCoscine() {
  console.log('Submitting request...'); // Add console log statement

  this.getProject(this.route.snapshot.params['id']);

  const url = `${environment.backend_url_prefix}/apiservices/execute-request-coscine/${this.title}/${this.currentProject.description}/`;

  this.http.get(url, { withCredentials: true }).subscribe(
    (response: any) => {
      console.log('RDMO Project created successfully'); //, response);
      console.log(response);
      //const responseData = response.json(); // Extract the response data
      // Add code to update the HTML with the response data
      // For example, you can assign the response to a variable and display it in the HTML template
      this.responseData = 'RDMO Project created successfully!'; //responseData;
    },
    (error) => {
      console.error('Error:', error);
    },
  );
}
textInput2: string;

responseData4: any; // Define the responseData property

submitAPIkeyCoscine() {
  console.log('Submitting request...');

  this.account.post_apikey_coscine(this.textInput2).subscribe(
    (response: any) => {
      console.log('API key submitted successfully:', response);
      this.responseData2 = 'API key submitted successfully!';
    },
    (error) => {
      console.error('Error:', error);
    },
  );
}

responseData: any; // Define the responseData property

performRequest() {
  console.log('Submitting request...'); // Add console log statement

  this.getProject(this.route.snapshot.params['id']);

  const url = `${environment.backend_url_prefix}/apiservices/execute-request/${this.currentProject.title}/${this.currentProject.description}/`;

  this.http.get(url, { withCredentials: true }).subscribe(
    (response: any) => {
      console.log('Project created successfully'); //, response);
      console.log(response);
      //const responseData = response.json(); // Extract the response data
      // Add code to update the HTML with the response data
      // For example, you can assign the response to a variable and display it in the HTML template
      this.responseData = 'Project created successfully!'; //responseData;
    },
    (error) => {
      console.error('Error:', error);
    },
  );
}

}
