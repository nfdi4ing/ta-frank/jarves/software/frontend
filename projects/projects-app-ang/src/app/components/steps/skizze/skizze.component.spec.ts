import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkizzeComponent } from './skizze.component';

describe('SkizzeComponent', () => {
  let component: SkizzeComponent;
  let fixture: ComponentFixture<SkizzeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SkizzeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SkizzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
