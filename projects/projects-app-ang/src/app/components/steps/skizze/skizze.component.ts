import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-skizze',
  templateUrl: './skizze.component.html',
  styleUrls: ['./skizze.component.css'],
})
export class SkizzeComponent implements OnInit {
  selectedItemNgModel: string;

  selectedItemFormControl = new FormControl();

  constructor() {}

  ngOnInit(): void {}
}
