import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Aufbereiten2Component } from './aufbereiten2.component';

describe('Aufbereiten2Component', () => {
  let component: Aufbereiten2Component;
  let fixture: ComponentFixture<Aufbereiten2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Aufbereiten2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Aufbereiten2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
