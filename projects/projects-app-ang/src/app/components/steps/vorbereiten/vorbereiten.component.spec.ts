import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VorbereitenComponent } from './vorbereiten.component';

describe('VorbereitenComponent', () => {
  let component: VorbereitenComponent;
  let fixture: ComponentFixture<VorbereitenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VorbereitenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VorbereitenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
