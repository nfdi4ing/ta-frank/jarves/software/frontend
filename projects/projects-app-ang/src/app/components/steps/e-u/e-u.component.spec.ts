import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EUComponent } from './e-u.component';

describe('EUComponent', () => {
  let component: EUComponent;
  let fixture: ComponentFixture<EUComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EUComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EUComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
