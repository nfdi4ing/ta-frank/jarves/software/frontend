import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FremddatenComponent } from './fremddaten.component';

describe('FremddatenComponent', () => {
  let component: FremddatenComponent;
  let fixture: ComponentFixture<FremddatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FremddatenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FremddatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
