import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivierenStepComponent } from './archivieren-step.component';

describe('ArchivierenStepComponent', () => {
  let component: ArchivierenStepComponent;
  let fixture: ComponentFixture<ArchivierenStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivierenStepComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArchivierenStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
