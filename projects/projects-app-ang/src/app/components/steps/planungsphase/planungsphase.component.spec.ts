import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanungsphaseComponent } from './planungsphase.component';

describe('PlanungsphaseComponent', () => {
  let component: PlanungsphaseComponent;
  let fixture: ComponentFixture<PlanungsphaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanungsphaseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanungsphaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
