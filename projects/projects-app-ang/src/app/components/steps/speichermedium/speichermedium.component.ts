import { Component, OnInit, Input} from '@angular/core';
import { AccountDataService } from '@services/account-data.service';
import { environment } from '@env';
import { HttpClient } from '@angular/common/http';
import { Project } from '../../../Project';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-speichermedium',
  templateUrl: './speichermedium.component.html',
  styleUrls: ['./speichermedium.component.css']
})
export class SpeichermediumComponent implements OnInit {

  @Input() currentProject: Project = {
    title: '',
    description: '',
  };
  projectId: string;

  constructor(
    private account: AccountDataService,
    private http: HttpClient,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.params['id'];
  }


  textInput: string;
  responseData: any; // Define the responseData property

  responseData2: any; // Define the responseData property

  submitAPIkey() {
    console.log('Submitting request...');

    this.account.post_apikey(this.textInput).subscribe(
      (response: any) => {
        console.log('API key submitted successfully:', response);
        this.responseData2 = 'API key submitted successfully!';
      },
      (error) => console.error('Error:', error),
    );
  }

  //api requests to coscine
  responseData3: any; // Define the responseData property

  performRequestCoscine() {
    console.log('Submitting request...'); // Add console log statement

    const url = `${environment.backend_url_prefix}/apiservices/execute-request-coscine/${this.currentProject.title}/${this.currentProject.description}/`;

    this.http.get(url, { withCredentials: true }).subscribe(
      (response: any) => {
        console.log('Project created successfully'); //, response);
        console.log(response);
        //const responseData = response.json(); // Extract the response data
        // Add code to update the HTML with the response data
        // For example, you can assign the response to a variable and display it in the HTML template
        this.responseData = 'Project created successfully!'; //responseData;
      },
      (error) => {
        console.error('Error:', error);
      },
    );
  }
  textInput2: string;

  responseData4: any; // Define the responseData property

  submitAPIkeyCoscine() {
    console.log('Submitting request...');

    this.account.post_apikey_coscine(this.textInput2).subscribe(
      (response: any) => {
        console.log('API key submitted successfully:', response);
        this.responseData2 = 'API key submitted successfully!';
      },
      (error) => {
        console.error('Error:', error);
      },
    );
  }
}
