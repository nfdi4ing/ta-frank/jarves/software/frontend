import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeichermediumComponent } from './speichermedium.component';

describe('SpeichermediumComponent', () => {
  let component: SpeichermediumComponent;
  let fixture: ComponentFixture<SpeichermediumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpeichermediumComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpeichermediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
