import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Dokumentation2Component } from './dokumentation2.component';

describe('Dokumentation2Component', () => {
  let component: Dokumentation2Component;
  let fixture: ComponentFixture<Dokumentation2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Dokumentation2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Dokumentation2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
