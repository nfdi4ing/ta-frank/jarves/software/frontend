import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Speichern2Component } from './speichern2.component';

describe('Speichern2Component', () => {
  let component: Speichern2Component;
  let fixture: ComponentFixture<Speichern2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Speichern2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Speichern2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
