import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DurchfuehrenComponent } from './durchfuehren.component';

describe('DurchfuehrenComponent', () => {
  let component: DurchfuehrenComponent;
  let fixture: ComponentFixture<DurchfuehrenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DurchfuehrenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DurchfuehrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
