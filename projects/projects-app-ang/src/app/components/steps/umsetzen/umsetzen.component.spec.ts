import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UmsetzenComponent } from './umsetzen.component';

describe('UmsetzenComponent', () => {
  let component: UmsetzenComponent;
  let fixture: ComponentFixture<UmsetzenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UmsetzenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UmsetzenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
