import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-metadaten',
  templateUrl: './metadaten.component.html',
  styleUrls: ['./metadaten.component.css']
})
export class MetadatenComponent implements OnInit {

  searchQuery: string;
  searchResults: any[];

  searchterminology() {
    // Konstruiere die Such-URL basierend auf der eingegebenen Suchanfrage
    const searchUrl =
      'https://terminology.nfdi4ing.de/ts/search?q=' +
      encodeURIComponent(this.searchQuery) +
      '&collection=NFDI4ING&page=1';
    // Öffne die Suchergebnisse in einem neuen Tab
    window.open(searchUrl, '_blank');
  }

  searchre3data() {
    // Konstruiere die Such-URL basierend auf der eingegebenen Suchanfrage
    const searchUrl =
      'https://www.re3data.org/search?query=' +
      encodeURIComponent(this.searchQuery);
    // Öffne die Suchergebnisse in einem neuen Tab
    window.open(searchUrl, '_blank');
  }

  constructor() { }

  ngOnInit(): void {
  }

}
