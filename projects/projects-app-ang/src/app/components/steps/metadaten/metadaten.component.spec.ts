import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadatenComponent } from './metadaten.component';

describe('MetadatenComponent', () => {
  let component: MetadatenComponent;
  let fixture: ComponentFixture<MetadatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetadatenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MetadatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
