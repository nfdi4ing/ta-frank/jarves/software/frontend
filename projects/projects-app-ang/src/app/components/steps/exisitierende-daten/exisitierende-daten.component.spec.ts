import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExisitierendeDatenComponent } from './exisitierende-daten.component';

describe('ExisitierendeDatenComponent', () => {
  let component: ExisitierendeDatenComponent;
  let fixture: ComponentFixture<ExisitierendeDatenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExisitierendeDatenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExisitierendeDatenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
