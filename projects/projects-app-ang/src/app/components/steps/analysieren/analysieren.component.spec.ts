import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysierenComponent } from './analysieren.component';

describe('AnalysierenComponent', () => {
  let component: AnalysierenComponent;
  let fixture: ComponentFixture<AnalysierenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalysierenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AnalysierenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
