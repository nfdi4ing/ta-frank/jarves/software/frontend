import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoerdervorgabenComponent } from './foerdervorgaben.component';

describe('FoerdervorgabenComponent', () => {
  let component: FoerdervorgabenComponent;
  let fixture: ComponentFixture<FoerdervorgabenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FoerdervorgabenComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(FoerdervorgabenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
