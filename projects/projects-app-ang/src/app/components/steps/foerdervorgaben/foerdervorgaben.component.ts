import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-foerdervorgaben',
  templateUrl: './foerdervorgaben.component.html',
  styleUrls: ['./foerdervorgaben.component.css'],
})
export class FoerdervorgabenComponent implements OnInit {
  constructor() {}

  searchQuery: string;

  ngOnInit(): void {}

  searchSherpa() {
    // Konstruiere die Such-URL basierend auf der eingegebenen Suchanfrage
    const searchUrl =
      'https://www.sherpa.ac.uk/cgi/search/funder/basic?funder_name-auto=' +
      encodeURIComponent(this.searchQuery) +
      '&_action_search=Search&screen=Search&funder_name-auto_merge=ALL';
    // Öffne die Suchergebnisse in einem neuen Tab
    window.open(searchUrl, '_blank');
  }
}
