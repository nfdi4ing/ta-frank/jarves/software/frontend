import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Durchfuehren2Component } from './durchfuehren2.component';

describe('Durchfuehren2Component', () => {
  let component: Durchfuehren2Component;
  let fixture: ComponentFixture<Durchfuehren2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Durchfuehren2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Durchfuehren2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
