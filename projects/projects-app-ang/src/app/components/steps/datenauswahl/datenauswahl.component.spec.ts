import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatenauswahlComponent } from './datenauswahl.component';

describe('DatenauswahlComponent', () => {
  let component: DatenauswahlComponent;
  let fixture: ComponentFixture<DatenauswahlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatenauswahlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatenauswahlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
