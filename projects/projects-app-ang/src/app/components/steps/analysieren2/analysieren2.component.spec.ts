import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Analysieren2Component } from './analysieren2.component';

describe('Analysieren2Component', () => {
  let component: Analysieren2Component;
  let fixture: ComponentFixture<Analysieren2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Analysieren2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Analysieren2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
