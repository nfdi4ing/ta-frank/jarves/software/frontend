import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AufbereitenComponent } from './aufbereiten.component';

describe('AufbereitenComponent', () => {
  let component: AufbereitenComponent;
  let fixture: ComponentFixture<AufbereitenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AufbereitenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AufbereitenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
