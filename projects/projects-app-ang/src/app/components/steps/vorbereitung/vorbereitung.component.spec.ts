import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VorbereitungComponent } from './vorbereitung.component';

describe('VorbereitungComponent', () => {
  let component: VorbereitungComponent;
  let fixture: ComponentFixture<VorbereitungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VorbereitungComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VorbereitungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
