import { Component, OnInit } from '@angular/core';
import { ProjectsDataSource } from '../../projects-material-table/projects-material-table.component';
import { ProjectAppService } from '@services/project-app.service';
import { MessageService } from '../../../message.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-rollen',
  templateUrl: './rollen.component.html',
  styleUrls: ['./rollen.component.css'],
})
export class RollenComponent implements OnInit {
  dataSource!: ProjectsDataSource;
  displayedColumns = ['name', 'e-mail', 'phone', 'role'];
  PeopleCount!: any;
  selectedItemNgModel: string;
  Name: string;
  Mail: string;
  Phone: string;
  Role: string;

  selectedItemFormControl = new FormControl();

  constructor(
    private projectAppService: ProjectAppService,
    private messageService: MessageService,
  ) {}

  ngOnInit(): void {
    this.PeopleCount = 0;
    this.dataSource = new ProjectsDataSource(
      this.projectAppService,
      this.messageService,
    );
  }

  getRollen(row: string) {}

  AssignRole(
    Name: String,
    Mail: string,
    Phone: string,
    Role: string): void {
      this.dataSource.assignRole(Name,Mail,Phone,Role)
      .subscribe();
  }
}
