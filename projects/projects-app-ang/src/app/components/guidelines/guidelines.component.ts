import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guidelines',
  templateUrl: './guidelines.component.html',
  styleUrls: ['./guidelines.component.css'],
})
export class GuidelinesComponent implements OnInit {
  constructor() {
    console.log('constructor() guidelines');
  }

  ngOnInit(): void {
    console.log('ngOnInit() guidelines');
  }

  tableData: any[] = [
    {
      column1: '',
      column2: '',
      column3: '',
      column4: '',
      column5: '',
      column6: '',
      column7: '',
    },
    {
      column1: '',
      column2: '',
      column3: '',
      column4: '',
      column5: '',
      column6: '',
      column7: '',
    },
    {
      column1: '',
      column2: '',
      column3: '',
      column4: '',
      column5: '',
      column6: '',
      column7: '',
    },
    {
      column1: '',
      column2: '',
      column3: '',
      column4: '',
      column5: '',
      column6: '',
      column7: '',
    },
  ];
}
