import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  constructor() {
    console.log('constructor() reset-password');
  }

  ngOnInit(): void {
    console.log('ngOnInit() reset-password');
  }
}
