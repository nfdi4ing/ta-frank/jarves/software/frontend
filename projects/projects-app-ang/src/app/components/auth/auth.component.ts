import { Component, OnInit } from '@angular/core';
//import { NbLoginComponent } from './login/login.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  constructor() {
    console.log('constructor() Auth');
  }

  /*
  constructor(private pageService: PageService) {
    this.pageService.setIsLandingPage(true);
    this.pageService.setIsMainPage(false);
  }*/

  ngOnInit(): void {
    console.log('ngOnInit() auth');
  }
}
