import { Router } from '@angular/router';
import { Component, ChangeDetectorRef, Inject } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import { PageService } from '../../../page.service';
import { NB_AUTH_OPTIONS, NbAuthService, NbAuthOptions } from '@nebular/auth';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent extends NbLoginComponent {
  constructor(
    service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) options: NbAuthOptions,
    cd: ChangeDetectorRef,
    router: Router,
    private pageService: PageService,
  ) {
    super(service, options, cd, router);
    this.pageService.setIsLandingPage(true);
    this.pageService.setIsMainPage(false);
  }

  /*ngOnInit(): void {
    super.ngOnInit();
  }*/
}

/**
 *
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/*import { ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NbAuthSocialLink } from '../../auth.options';
import { NbAuthService } from '../../services/auth.service';
import * as i0 from "@angular/core";
export declare class NbLoginComponent {
    protected service: NbAuthService;
    protected options: {};
    protected cd: ChangeDetectorRef;
    protected router: Router;
    redirectDelay: number;
    showMessages: any;
    strategy: string;
    errors: string[];
    messages: string[];
    user: any;
    submitted: boolean;
    socialLinks: NbAuthSocialLink[];
    rememberMe: boolean;
    constructor(service: NbAuthService, options: {}, cd: ChangeDetectorRef, router: Router);
    login(): void;
    getConfigValue(key: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<NbLoginComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<NbLoginComponent, "nb-login", never, {}, {}, never, never>;
}*/

/*
import { Router } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import { PageService } from '../../../page.service';
import { NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})

export class NgxLoginComponent extends NbLoginComponent  {

  constructor(service: NbAuthService, options: {}, cd: ChangeDetectorRef, routes: Router){
    super(service, options, cd, routes);
  }
  
  }
*/

//export class NgxLoginComponent implements OnInit {

/*constructor(private pageService: PageService) {
      this.pageService.setIsLandingPage(true);
      this.pageService.setIsMainPage(false);
    }*/

// ngOnInit(): void {
//}

// }

/*
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NbAuthService, NbLoginComponent } from '@nebular/auth';
import { NbLoginComponent } from '@nebular/auth';
import { PageService } from '../../../page.service';

interface LoginOptions {
  option1: string;
  option2: number;
  // add other properties as needed
}

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent extends NbLoginComponent {

  constructor(private pageService: PageService, service: NbAuthService, options: LoginOptions, cd: ChangeDetectorRef, router: Router) {
    super(service, options, cd, router);
    this.pageService.setIsLandingPage(true);
    this.pageService.setIsMainPage(false);
    // constructor implementation
  }

  // other methods and properties
}*/
