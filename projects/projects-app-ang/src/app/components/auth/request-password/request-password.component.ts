import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-password',
  templateUrl: './request-password.component.html',
  styleUrls: ['./request-password.component.css'],
})
export class RequestPasswordComponent implements OnInit {
  constructor() {
    console.log('constructor() request-password');
  }

  ngOnInit(): void {
    console.log('ngOnInit() request-password');
  }
}
