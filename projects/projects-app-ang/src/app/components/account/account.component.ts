import { Component, OnInit, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountDataService } from '@services/account-data.service';
import { Observable } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  apiKey = 'Loading...';
  username = 'Loading...';
  email = 'Loading...';
  apiKey_coscine = 'Loading...';
  emailAccount = 'Loading...';
  userId = 'Loading...';

  constructor(
    private http: HttpClient,
    private account: AccountDataService,
    private changedetector : ChangeDetectorRef,
  ) {}

  textInput: string;
  responseData: any; // Define the responseData property

  textInput2: string;
  responseData2: any; // Define the responseData property

  textInput3: string;
  responseData3: any; // Define the responseData property

  async ngOnInit(){
    try {
      const [emailAccountValue, userIdValue] = await Promise.all([
        this.account.get_email().subscribe(
          (email_account) => {
            this.changedetector.markForCheck() // This line makes sure that the value is updated once loaded
            this.emailAccount = email_account
          }
        ),
        this.account.get_user_id().subscribe(
          (user_id) => {
            this.changedetector.markForCheck() // This line makes sure that the value is updated once loaded
            this.userId = user_id
          }
        )
      ]);
  
      // Await the remaining calls if necessary
      await this.account.get_apikey().subscribe(
        (api_key) => {
          this.changedetector.markForCheck() // This line makes sure that the value is updated once loaded
          this.apiKey = api_key
        }
      );
      await this.account.get_apikey_coscine().subscribe(
        (api_key_coscine) => {
          this.changedetector.markForCheck() // This line makes sure that the value is updated once loaded
          this.apiKey_coscine = api_key_coscine
        }
      );
      this.account.get_logged_in_user().subscribe(
        (response) => {
          this.changedetector.markForCheck() // This line makes sure that the value is updated once loaded
          this.username = response.username;
        }
      );
  /* 
      this.apiKey = apiKeyValue;
      this.apiKey_coscine = apiKeyCoscineValue;
      this.username = loggedInUser.username; */
      // this.email = loggedInUser.email (if needed)
    } catch (error) {
      console.error('Error:', error);
    }
    
  }

  
/* 
  loadData() { *//* 
    this.account.get_email().subscribe(
      (email_account) => (this.emailAccount = email_account),
      (error) => console.error('Error:', error),
    );

    this.account.get_user_id().subscribe(
        (user_id) => (this.userId = user_id),
        (error) => console.error('Error:', error),
    ); */
    
      /* 
    this.account.get_apikey().subscribe(
      (api_key) => (this.apiKey = api_key),
      (error) => console.error('Error:', error),
    );

    this.account.get_apikey_coscine().subscribe(
      (api_key_coscine) => (this.apiKey_coscine = api_key_coscine),
      (error) => console.error('Error:', error),
    );

    this.account.get_logged_in_user().subscribe(
      (response) => {
        this.username = response.username;
        //this.email = response.email;
      },
      (error) => console.error('Error:', error),
    );
  } */

  submitAPIkey() {
    console.log('Submitting request...');

    this.account.post_apikey(this.textInput).subscribe(
      (response: any) => {
        console.log('API key submitted successfully:', response);
        this.responseData = 'API key submitted successfully!';
      },
      (error) => console.error('Error:', error),
    );
  }

  submitAPIkeyCoscine() {
    console.log('Submitting request...');

    this.account.post_apikey_coscine(this.textInput2).subscribe(
      (response: any) => {
        console.log('API key submitted successfully:', response);
        this.responseData2 = 'API key submitted successfully!';
      },
      (error) => {
        console.error('Error:', error);
      },
    );
  }

  postEmail() {
    console.log('Submitting request...');

    this.account.post_email(this.textInput3).subscribe(
      (response: any) => {
        console.log('Email postet successfully:', response);
        this.responseData3 = 'Email postet successfully!';
      },
      (error) => console.error('Error:', error),
    );
  }
}
