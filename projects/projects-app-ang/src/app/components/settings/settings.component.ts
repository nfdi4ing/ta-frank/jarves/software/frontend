import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbThemeService } from '@nebular/theme';
import { environment } from '@env';
import { AccountDataService } from '../../services/account-data.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  searchQuery: string;
  searchResults: any[];

  constructor(
    private http: HttpClient,
    private themeService: NbThemeService,
    private account: AccountDataService,
  ) {}

  search() {
    // Konstruiere die Such-URL basierend auf der eingegebenen Suchanfrage
    const searchUrl = 'https://terminology.nfdi4ing.de/ts/search?q=' + encodeURIComponent(this.searchQuery) + '&collection=NFDI4ING&page=1';
     // Öffne die Suchergebnisse in einem neuen Tab
    window.open(searchUrl, '_blank');
  }


  ngOnInit(): void {}

  toggleDarkMode(value: boolean): void {
    console.log("toggled to state " + value);

    if (value) {
      this.themeService.changeTheme('dark');
    } else {
      this.themeService.changeTheme('default');
    }

    this.account.post_darkmode(value).subscribe(
      (response: any) => { console.log('Got response:', response); },
      error => { console.error('Encountered error:', error); },
    );
  }
}