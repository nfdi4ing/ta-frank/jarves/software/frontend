import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErhebungComponent } from './erhebung.component';

describe('ErhebungComponent', () => {
  let component: ErhebungComponent;
  let fixture: ComponentFixture<ErhebungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErhebungComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ErhebungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
