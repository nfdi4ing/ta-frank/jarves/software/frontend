import { AfterViewInit, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { StepperService } from '@services/stepper.service';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { FeedbackService } from '@services/feedback.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-erhebung',
  templateUrl: './erhebung.component.html',
  styleUrls: ['./erhebung.component.css']
})
export class ErhebungComponent implements AfterViewInit {

  @ViewChild('stepperErhebung') erhebungStepper: NbStepperComponent;

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log('Keydown event:', event.key); // Log the keydown event

    this.stepperService.getCurrentStep().pipe(take(1)).subscribe(currentStep => {
      // console.log('Current step:', currentStep); // Log the current step

      if (currentStep !== 3) {
        return;
      }

      switch (event.key) {
        case 'ArrowLeft':
          event.preventDefault();
          // console.log('Navigating to previous step'); // Log the navigation action
          this.erhebungStepper.previous();
          break;
        case 'ArrowRight':
          event.preventDefault();
          // console.log('Navigating to next step'); // Log the navigation action
          this.erhebungStepper.next();
          break;
      }
    });
  }

  @ViewChild('stepperMain') stepper8: NbStepperComponent;

  constructor(
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
  ) { }

  mainStepper: NbStepperComponent;
  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.erhebungStepper);
  }

}
