import { Component, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { NbStepperComponent } from '@nebular/theme';
import { StepperService } from '@services/stepper.service';
import { FeedbackService } from '@services/feedback.service';
import { take } from 'rxjs/operators';


@Component({
  selector: 'app-analyse',
  templateUrl: './analyse.component.html',
  styleUrls: ['./analyse.component.css']
})
export class AnalyseComponent implements AfterViewInit {
  @ViewChild('stepperAnalyse') analyseStepper: NbStepperComponent;

  focus: boolean = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.focus = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.focus = false;
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log('Keydown event:', event.key); // Log the keydown event

    this.stepperService.getCurrentStep().pipe(take(1)).subscribe(currentStep => {
      // console.log('Current step:', currentStep); // Log the current step

      if (currentStep !== 2) {
        return;
      }

      switch (event.key) {
        case 'ArrowLeft':
          event.preventDefault();
          // console.log('Navigating to previous step'); // Log the navigation action
          this.analyseStepper.previous();
          break;
        case 'ArrowRight':
          event.preventDefault();
          // console.log('Navigating to next step'); // Log the navigation action
          this.analyseStepper.next();
          break;
      }
    });
  }

  @ViewChild('stepperMain') stepper8: NbStepperComponent;

  constructor(
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
  ) { }

  mainStepper: NbStepperComponent;
  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.analyseStepper);
  }

}
