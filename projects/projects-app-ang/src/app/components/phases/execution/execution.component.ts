import { Component, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { NbStepperComponent, NbTreeGridDataSource, NbTreeGridDataSourceBuilder } from '@nebular/theme';
import { StepperService } from '@services/stepper.service';
import { FeedbackService } from '@services/feedback.service';
import { take } from 'rxjs/operators';


interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
}
interface FSEntry {
  name: string;
  id: number;
  type: string;
}

@Component({
  selector: 'app-execution',
  templateUrl: './execution.component.html',
  styleUrls: ['./execution.component.css']
})
export class ExecutionComponent implements AfterViewInit {
  @ViewChild('stepperExecution') executionStepper: NbStepperComponent;
  

  focus: boolean = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.focus = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.focus = false;
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log('Keydown event:', event.key); // Log the keydown event

    this.stepperService.getCurrentStep().pipe(take(1)).subscribe(currentStep => {
      // console.log('Current step:', currentStep); // Log the current step

      if (currentStep !== 2) {
        return;
      }

      switch (event.key) {
        case 'ArrowLeft':
          event.preventDefault();
          // console.log('Navigating to previous step'); // Log the navigation action
          this.executionStepper.previous();
          break;
        case 'ArrowRight':
          event.preventDefault();
          // console.log('Navigating to next step'); // Log the navigation action
          this.executionStepper.next();
          break;
      }
    });
  }

  @ViewChild('stepperMain') stepper8: NbStepperComponent;

  dataSource: NbTreeGridDataSource<FSEntry>;
  data: TreeNode<FSEntry>[] = [
    {
      data: { name: 'Finding1', id: 1, type: 'finding'},
      children: [
        {
          data: { name: 'Artifact 1', id: 2, type: 'artifact'},
        },
        {
          data: { name: 'Artifact 2', id: 3, type: 'artifact'},
        },
        {
          data: { name: 'Artifact 3', id: 4, type: 'artifact'},
        },
      ],
    },
    {
      data: { name: 'Finding2', id: 5, type: 'finding'},
    },
    {
      data: { name: 'Finding3', id: 6, type: 'finding'},
    },
  ];
  allColumns = ['name'];
  handleTreeItemClick(event: TreeNode<FSEntry>) {
    console.log(event);
    this.selectedNode = event.data;
  }

  selectedNode: FSEntry

  constructor(
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
    private dataSourceBuilder: NbTreeGridDataSourceBuilder<FSEntry>
  ) {
    this.dataSource = this.dataSourceBuilder.create(this.data);
  }


  mainStepper: NbStepperComponent;
  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.executionStepper);
  }

}
