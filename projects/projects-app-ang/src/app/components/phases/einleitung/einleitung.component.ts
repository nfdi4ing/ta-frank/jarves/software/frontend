import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../../../Project';

@Component({
  selector: 'app-einleitung',
  templateUrl: './einleitung.component.html',
  styleUrls: ['./einleitung.component.css']
})
export class EinleitungComponent implements OnInit {
  @Input() currentProject: Project = {
    title: '',
    description: '',
  };

  constructor() { }

  ngOnInit(): void {
  }

}
