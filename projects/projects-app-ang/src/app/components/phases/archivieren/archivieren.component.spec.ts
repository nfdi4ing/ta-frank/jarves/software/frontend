import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivierenComponent } from './archivieren.component';

describe('ArchivierenComponent', () => {
  let component: ArchivierenComponent;
  let fixture: ComponentFixture<ArchivierenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivierenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArchivierenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
