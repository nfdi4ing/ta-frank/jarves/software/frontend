import { Component, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { StepperService } from '@services/stepper.service';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { FeedbackService } from '@services/feedback.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-archivieren',
  templateUrl: './archivieren.component.html',
  styleUrls: ['./archivieren.component.css']
})
export class ArchivierenComponent implements AfterViewInit {

  @ViewChild('stepperMain') stepper8: NbStepperComponent;

  @ViewChild('stepperArchivieren') archivierenStepper: NbStepperComponent;

  focus: boolean = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.focus = true;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.focus = false;
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log('Keydown event:', event.key); // Log the keydown event

    this.stepperService.getCurrentStep().pipe(take(1)).subscribe(currentStep => {
      // console.log('Current step:', currentStep); // Log the current step

      if (currentStep !== 4) {
        return;
      }

      switch (event.key) {
        case 'ArrowLeft':
          event.preventDefault();
          // console.log('Navigating to previous step'); // Log the navigation action
          this.archivierenStepper.previous();
          break;
        case 'ArrowRight':
          event.preventDefault();
          // console.log('Navigating to next step'); // Log the navigation action
          this.archivierenStepper.next();
          break;
      }
    });
  }

  constructor(
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
  ) { }

  mainStepper: NbStepperComponent;
  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.archivierenStepper);
  }
}
