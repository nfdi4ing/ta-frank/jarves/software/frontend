import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanenComponent } from './planen.component';

describe('PlanenComponent', () => {
  let component: PlanenComponent;
  let fixture: ComponentFixture<PlanenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
