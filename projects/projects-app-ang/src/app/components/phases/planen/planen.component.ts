import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { StepperService } from '@services/stepper.service';
import { NbStepperComponent, NbStepComponent } from '@nebular/theme';
import { FeedbackService } from '@services/feedback.service';
import { HostListener } from '@angular/core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-planen',
  templateUrl: './planen.component.html',
  styleUrls: ['./planen.component.css']
})
export class PlanenComponent implements AfterViewInit {

  constructor(
    public stepperService: StepperService,
    public feedbackService: FeedbackService,
  ) { }

  ngAfterViewInit() {
    this.stepperService.setMainStepper(this.planenStepper);
  }

  @ViewChild('stepperPlanen') planenStepper: NbStepperComponent;

  @HostListener('window:keydown', ['$event'])
handleKeyboardEvent(event: KeyboardEvent) {
  // console.log('Keydown event:', event.key); // Log the keydown event

  this.stepperService.getCurrentStep().pipe(take(1)).subscribe(currentStep => {
    // console.log('Current step:', currentStep); // Log the current step

    if (currentStep !== 1) {
      return;
    }

    switch (event.key) {
      case 'ArrowLeft':
        event.preventDefault();
        // console.log('Navigating to previous step'); // Log the navigation action
        this.planenStepper.previous();
        break;
      case 'ArrowRight':
        event.preventDefault();
        // console.log('Navigating to next step'); // Log the navigation action
        this.planenStepper.next();
        break;
    }
  });
}

  @ViewChild('stepperMain') stepper8: NbStepperComponent;

  mainStepper: NbStepperComponent;

  navigate(direction: string) {
    this.stepperService.getCurrentStep().subscribe(currentStep => {
      switch (direction) {
        case 'ArrowRight':
          if (currentStep === this.planenStepper.selectedIndex) {
            this.planenStepper.next();
          }
          break;
        case 'ArrowLeft':
          if (currentStep === this.planenStepper.selectedIndex) {
            this.planenStepper.previous();
          }
          break;
      }
    });
  }
}
