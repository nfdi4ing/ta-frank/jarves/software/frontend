import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-c-language',
  templateUrl: './c-language.component.html',
  styleUrls: ['./c-language.component.css'],
})
export class CLanguageComponent implements OnInit {
  constructor(public translate: TranslateService) {
    // Register translation languages
    translate.addLangs(['🇬🇧', '🇩🇪']);
    // Set default language
    translate.setDefaultLang('🇬🇧');
  }

  //Switch language
  translateLanguageTo(lang: string) {
    this.translate.use(lang);
  }
  ngOnInit(): void {
    console.log('ngOnInit() c-language');
  }
}
