import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CBarComponent } from './c-bar.component';

describe('CBarComponent', () => {
  let component: CBarComponent;
  let fixture: ComponentFixture<CBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CBarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
