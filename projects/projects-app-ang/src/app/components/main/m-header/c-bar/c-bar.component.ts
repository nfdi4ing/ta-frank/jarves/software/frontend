import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c-bar',
  templateUrl: './c-bar.component.html',
  styleUrls: ['./c-bar.component.css'],
})
export class CBarComponent implements OnInit {
  constructor() {
    console.log('constructor() c-bar');
  }

  ngOnInit(): void {
    console.log('ngOnInit() c-bar');
  }
}
