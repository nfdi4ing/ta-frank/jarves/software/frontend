import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-c-landing',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './c-landing.component.html',
  styleUrls: ['./c-landing.component.css'],
})
export class CLandingComponent {}
