import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CLandingComponent } from './c-landing.component';

describe('CLandingComponent', () => {
  let component: CLandingComponent;
  let fixture: ComponentFixture<CLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CLandingComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
