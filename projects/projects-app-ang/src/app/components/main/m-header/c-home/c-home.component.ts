import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c-home',
  templateUrl: './c-home.component.html',
  styleUrls: ['./c-home.component.css'],
})
export class CHomeComponent implements OnInit {
  constructor() {
    console.log('constructor() c-home');
  }

  ngOnInit(): void {
    console.log('ngOnInit() c-home');
  }
}
