import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NbSidebarService } from '@nebular/theme';

import { NbAuthService } from '@nebular/auth';

@Component({
  selector: 'app-m-header',
  templateUrl: './m-header.component.html',
  styleUrls: ['./m-header.component.css'],
})
export class MHeaderComponent implements OnInit {
  user = {};

  constructor(
    private authService: NbAuthService,

    public translate: TranslateService,
    private sidebarService: NbSidebarService,
  ) {
    // Register translation languages
    translate.addLangs(['🇬🇧', '🇩🇪']);
    // Set default language
    translate.setDefaultLang('🇬🇧');

    this.authService.onTokenChange().subscribe((token: any) => {
      if (token.isValid()) {
        this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable
      }
    });
  }

  //Switch language
  translateLanguageTo(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit(): void {
    console.log('ngOnInit() m-header');
  }

  toggle() {
    this.sidebarService.toggle(false, 'left');
  }
}

/*
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProjectsAppAng';

  constructor(
    public translate: TranslateService
  ){
    // Register translation languages
    translate.addLangs(['🇬🇧', '🇩🇪']);
    // Set default language
    translate.setDefaultLang('🇬🇧');
  } 

  //Switch language
  translateLanguageTo(lang: string) {
    this.translate.use(lang);
  }

}
*/
