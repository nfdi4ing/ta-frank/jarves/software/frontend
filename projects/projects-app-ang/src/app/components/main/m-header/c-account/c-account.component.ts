import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c-account',
  templateUrl: './c-account.component.html',
  styleUrls: ['./c-account.component.css'],
})
export class CAccountComponent implements OnInit {
  constructor() {
    console.log('constructor() c-account');
  }

  ngOnInit(): void {
    console.log('ngOnInit() c-account');
  }
}
