import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CExitComponent } from './c-exit.component';

describe('CExitComponent', () => {
  let component: CExitComponent;
  let fixture: ComponentFixture<CExitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CExitComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CExitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
