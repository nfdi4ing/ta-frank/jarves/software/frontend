import { Component, OnInit } from '@angular/core';
import { environment } from '@env';

@Component({
  selector: 'app-c-exit',
  templateUrl: './c-exit.component.html',
  styleUrls: ['./c-exit.component.css'],
})
export class CExitComponent implements OnInit {
  backend_url: string;

  constructor() {
    console.log('constructor() c-exit');
  }

  ngOnInit(): void {
    this.backend_url = environment.backend_url_prefix;
  }
}
