import { PageService } from '../../page.service';
import { environment } from '@env';
import { FeedbackService } from '@services/feedback.service';
import { NeedsForHelp } from '../../needs-for-help';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { AccountDataService } from '../../services/account-data.service'; // Stellen Sie sicher, dass Sie den richtigen Pfad zur AccountDataService-Datei angeben

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  SendNeedForHelpResponseData: any; // To store the response data
  NeedsForHelpResponseData: any; // To store the response data
  NumberOfNeedsForHelp: any; // To store the response data
  needsforhelp!: NeedsForHelp[];
  //dataSource!: ProjectsDataSource;

  /**
   * Boolean checking if main stuff shall be visible
   */

  @ViewChild('input') input!: ElementRef;
  isDarkMode: boolean;

  constructor(
    private pageService: PageService,
    private FeedbackService: FeedbackService,
    private cdr: ChangeDetectorRef,
    private account: AccountDataService,
  ) {
    this.pageService.setIsLandingPage(true);
    this.pageService.setIsMainPage(false);
    this.isDarkMode = this.account.isDarkModeEnabled();
  }

  ngOnInit(): void {
    this.GetNeedsForHelp();
    console.log('Data:');
    if (this.isDarkMode) {
      console.log('Dark Mode ist aktiviert');
    } else {
      console.log('Dark Mode ist deaktiviert');
    }
  }

  /*   ngAfterViewInit(): void {

    //this.GetNeedsForHelp();

    // server-side search
    fromEvent(this.input.nativeElement,'keyup')
    .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
            // Important: First Page is PageNumber=1, not 0--> Causes weird behaviour in backend
            this.GetNeedsForHelp();
        })
    )
    .subscribe();
  } */

  toggleDarkMode(value: boolean): void {
    this.account.setDarkModeEnabled(value);
  }

  redirectLogin(): void {
    window.location.href = `${environment.backend_url_prefix}/accounts/orcid/login/`;
  }

  GetNeedsForHelp(): void {
    this.FeedbackService.getNeedsForHelp().subscribe(
      (response) => {
        // Handle the response from the server
        console.log('Response:', response);
        this.NeedsForHelpResponseData = response; // Assign the response to a variable for display
        this.updateNumberOfNeedsForHelp();
        console.log('this.NeedsForHelpResponseData');
        console.log(this.NeedsForHelpResponseData[0].number);
        console.log(this.NumberOfNeedsForHelp);
      },
      (error) => {
        // Handle any errors that occurred during the request
        console.error('Error:', error);
      },
    );
  }

  ClickNeedForHelp(): void {
    this.SendNeedForHelp();
    setTimeout(() => {
      this.GetNeedsForHelp();
      this.cdr.detectChanges();
    }, 500); // 500 milliseconds (0.5 seconds)
  }

  SendNeedForHelp(): void {
    console.log('SendNeedForHelp');
    this.FeedbackService.create().subscribe(
      (response) => {
        // Handle the response from the server
        console.log('Response:', response);
        this.SendNeedForHelpResponseData = response; // Assign the response to a variable for display
      },
      (error) => {
        // Handle any errors that occurred during the request
        console.error('Error:', error);
      },
    );
  }

  updateNumberOfNeedsForHelp(): void {
    this.NumberOfNeedsForHelp = this.NeedsForHelpResponseData[0].number;
    this.cdr.detectChanges();
  }
  /*   getNeedForHelp(id: string): void {
    this.FeedbackService.get(id)
      .subscribe({
        next: (data) => {
          this.currentProject = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  } */
}
