import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  ElementRef,
} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';

import { BehaviorSubject, Observable, merge, fromEvent } from 'rxjs';
import {
  finalize,
  tap,
  debounceTime,
  distinctUntilChanged,
  map,
} from 'rxjs/operators';

import { HttpResponse } from '@angular/common/http';

import { ProjectAppService } from '../../services/project-app.service';
import { Project } from '../../Project';
import { MessageService } from '../../message.service';
import { MatSort } from '@angular/material/sort';

import { environment } from '@env';

@Component({
  selector: 'app-projects-material-table',
  templateUrl: './projects-material-table.component.html',
  styleUrls: ['./projects-material-table.component.css'],
})
export class ProjectsMaterialTableComponent implements OnInit, AfterViewInit {
  project!: Project[];
  dataSource!: ProjectsDataSource;
  displayedColumns = ['title', 'status', 'description', 'id'];
  pageSizeOptions = [3, 5, 10, 100];
  attribute: string;
  ProjectCount!: any;
  searchTerm: string;
  // maybe project count, shall pass the count of the projects to the html file
  // myarray: Project[]; // needs to be so we can get the length of the projectslist,
  // length$: Observable<Project[]> // gets the projects from the backend somehow, no real clue how, cant be used directly
  frontend_url: string;

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('input') input!: ElementRef;

  constructor(
    private projectAppService: ProjectAppService,
    private messageService: MessageService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.frontend_url = environment.frontend_url_prefix;

    this.project = this.route.snapshot.data['project'];
    this.dataSource = new ProjectsDataSource(
      this.projectAppService,
      this.messageService,
    );

    //this.ProjectCount = this.dataSource.
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.getAllProjects()

    // Load Projects with Filter, SortOrder, PageNumber and PageSize
    // Important: First Page is PageNumber=1, not 0--> Causes weird behaviour in backend

    this.dataSource.loadProjects('', 'asc', 0, 5, 'title');

    console.log('DataSource:');
    console.log(this.dataSource);

    console.log('Data Source Project Count:');
    console.log(this.dataSource.ProjectCount);
    this.ProjectCount = this.dataSource.ProjectCount;

    console.log('Project Count:');
    console.log(this.ProjectCount);

    console.log('ngOnInit()');
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          // Important: First Page is PageNumber=1, not 0--> Causes weird behaviour in backend
          this.paginator.pageIndex = 0;
          this.loadProjectsPage();
        }),
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    // rauskriegen was sortiert wird und zu datasource passen (sort_by)

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
          this.loadProjectsPage();
        }),
      )
      .subscribe();
  }

  onSearchInput(value: string): void {
    const searchBox = document.querySelector('.mat-form-field');
    if (searchBox) {
      if (value) {
        searchBox.classList.add('search-box-active');
      } else {
        searchBox.classList.remove('search-box-active');
      }
    }
  }

  loadProjectsPage() {
    //this.dataSource.getAllProjects()

    this.dataSource.loadProjects(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.attribute,
    );
    console.log('this.paginator.pageIndex');
    console.log(this.paginator.pageIndex);
    console.log('this.paginator.pageSize');
    console.log(this.paginator.pageSize);
  }

  getProject(row = '') {
    console.log('getProject');
    console.log(row);
  }
}

export class ProjectsDataSource implements DataSource<Project> {
  private projectsSubject = new BehaviorSubject<Project[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private projectCountSubject = new BehaviorSubject<string>('');

  public loading$ = this.loadingSubject.asObservable();
  public ProjectCount = this.projectCountSubject.asObservable();

  projects: Project[] = [];

  constructor(
    private projectService: ProjectAppService,
    private messageService: MessageService,
  ) {}

  // rm collectionViewer: CollectionViewer
  connect(): Observable<Project[]> {
    return this.projectsSubject.asObservable();
  }

  // rm collectionViewer: CollectionViewer
  disconnect(): void {
    this.projectsSubject.complete();
    this.loadingSubject.complete();
    this.projectCountSubject.complete();
  }

  loadProjects(
    filter = '',
    sortDirection = 'asc',
    pageIndex: number,
    pageSize: number,
    attribute: string,
  ) {
    this.loadingSubject.next(true);
    console.log('pageIndex');
    console.log(pageIndex);
    console.log('pageSize');
    console.log(pageSize);

    this.projectService
      .findProjects(filter, sortDirection, pageIndex, pageSize, attribute)
      .pipe(
        map((response: HttpResponse<Project[]>) => {
          const projects: Project[] = response.body || [];
          //this.projectCountSubject.next(response.headers.get('projectcount') || '');
          const projectCount = response.headers.get('projectcount') || '';
          this.projectsSubject.next(projects);
          this.projectCountSubject.next(projectCount);
          //this.ProjectCount=response.headers.get('projectcount');
          // count => this.projectCountSubject = response.headers.get('projectcount') ?? 'LEER';
          return projects;
        }),
        finalize(() => this.loadingSubject.next(false)),
        tap(() => this.log(`projects-material-table.components.ts: finalize`)),
      )
      .subscribe();
  }
  /* this.projectService.findProjects(
          filter, 
          sortDirection,
          pageIndex, 
          pageSize,
          attribute,
          ).pipe(
            map((response: HttpResponse<Project[]>) => {
              const projects: Project[] = response.body || [];
              this.projectCountSubject.next(response.headers.get('ProjectCount') || '');
              //this.ProjectCount=response.headers.get('projectcount');
              // count => this.projectCountSubject = response.headers.get('projectcount') ?? 'LEER';
              return projects
            }),
            finalize(() => this.loadingSubject.next(false)),
            tap(_ => this.log(`projects-material-table.components.ts: finalize`)),
        )
        .subscribe(
            project => this.projectsSubject.next(project),
    ) */

  getAllProjects(): void {
    this.projectService
      .getAll()
      .subscribe((project) => this.projectsSubject.next(project));
  }

  createProject(data: Project): void {
    this.projectService.create(data);
  }

  assignRole(
    Name: String,
    Mail: string,
    Phone: string,
    Role: string): Observable<any> {

      const data={
        name: Name,
        mail: Mail,
        phone: Phone,
        role: Role
      };

      return this.projectService.assignRole(data)
    }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }
}
