import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsMaterialTableComponent } from './projects-material-table.component';

describe('ProjectsMaterialTableComponent', () => {
  let component: ProjectsMaterialTableComponent;
  let fixture: ComponentFixture<ProjectsMaterialTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectsMaterialTableComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ProjectsMaterialTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
