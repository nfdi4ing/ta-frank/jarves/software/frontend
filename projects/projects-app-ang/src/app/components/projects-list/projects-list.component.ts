import { Component, OnInit } from '@angular/core';

import { Project } from '../../Project';
import { ProjectAppService } from '../../services/project-app.service';
import { MessageService } from '../../message.service';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css'],
})
export class ProjectsListComponent implements OnInit {
  projects: Project[] = [];
  currentProject: Project = {};
  currentIndex = -1;
  title = '';
  constructor(
    private projectService: ProjectAppService,
    private messageService: MessageService,
  ) {}

  ngOnInit(): void {
    this.retrieveProjects();
  }

  // Fetches all projects from the service
  retrieveProjects(): void {
    this.projectService
      .getAll()
      .subscribe((projects) => (this.projects = projects));
  }

  // refreshes the list. unclear if needed
  refreshList(): void {
    this.retrieveProjects();
    this.currentProject = {};
    this.currentIndex = -1;
  }

  // Sets the active project to index
  setActiveProject(project: Project, index: number): void {
    this.currentProject = project;
    this.currentIndex = index;
  }

  /** Django Viewset needs project ID to delete a project. Delete all is not allowed. Either dont impplement or do a for loop.
  removeAllProjects(): void {
    this.projectService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }
  */

  // Fetches all projects with fitting title from the service
  searchTitle(): void {
    this.currentProject = {};
    this.currentIndex = -1;
    this.projectService
      .findByTitle(this.title)
      .subscribe((projects) => (this.projects = projects));
  }
}
