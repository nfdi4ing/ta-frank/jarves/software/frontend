import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '@services/feedback.service';

@Component({
  selector: 'app-create-policy',
  templateUrl: './create-policy.component.html',
  styleUrls: ['./create-policy.component.css']
})
export class CreatePolicyComponent implements OnInit {

  constructor(
    public feedbackService: FeedbackService
  ) { }

  ngOnInit(): void {
  }

}
