import { CommonModule } from '@angular/common';
import { Component, Input, type OnInit } from '@angular/core';
import { PolicyApiService } from '@services/policy-api.service';

@Component({
  selector: 'app-project-policy-table',
  templateUrl: './project-policy-table.component.html',
  styleUrls: ['./project-policy-table.component.css'],
})
export class ProjectPolicyTableComponent implements OnInit {
  @Input()
  projectId: string;
  projectPolicies: any[] = [];
  displayedColumns = ['name', 'dmp_url', 'dmp_score', 'storage_url', 'storage_score'];

  constructor(private policyApiService: PolicyApiService) {}

  ngOnInit(): void {
    this.policyApiService.getProjectPolicies(this.projectId).subscribe({
      next: (data) => {
        console.log(data);
        this.projectPolicies = data;
      },
    });
  }

  isTableEmpty(): boolean {
    return !this.projectPolicies || this.projectPolicies.length === 0;
  }
}
