import { Component } from '@angular/core';
import { ProjectAppService } from '../../services/project-app.service';
import { ProjectsDataSource } from '../projects-material-table/projects-material-table.component';
import { MessageService } from '../../message.service';
import { environment } from '@env';
import { AccountDataService } from '@services/account-data.service';
import { University } from '@services/Interfaces/University';
import { Institute } from '@services/Interfaces/Institute';
import { PolicyApiService } from '@services/policy-api.service';
import { Funding } from '@services/Interfaces/Funding';
import { Router } from '@angular/router';


interface Project {
  title: string; //
  short_title: string; //
  displayName: string; //
  description: string; //
  principleInvestigators: string[]; // Array of strings //
  start_date: string; //
  endDate: string; //
  disciplines: string[]; // Array of strings //
  organizations: string[]; // Array of strings //
  keywords: string; //
  visibility: string; //
  grantid: string; //
  //
  projectName: string; //
  catalog: number; //
  user_or_email: string;
  role: string;
  status: string;
}
@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css'],
})
export class AddProjectComponent {
  data = MOCK_DATA;
  universities: University[] = [];
  institutes: Institute[] = [];
  fundings: Funding[] = [];
  selectedUniversityId: number;
  selectedInstituteId: number;
  selectedFundingId: number;

  project: Project = {
    title: '',
    short_title: '',
    start_date: '',
    endDate: '',
    description: '',
    displayName: '',
    keywords: '',
    projectName: '',
    principleInvestigators: [],
    disciplines: [],
    organizations: [],
    visibility: '',
    grantid: '',
    user_or_email: '',
    catalog: 0,
    role: '',
    status: ''
  };

  savedContent: Project = { ...this.project };
  frontend_url: string;
  dataSource!: ProjectsDataSource;

  constructor(
    private projectAppService: ProjectAppService,
    private messageService: MessageService,
    private accountDataService : AccountDataService,
    private policyApiService: PolicyApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.frontend_url = environment.frontend_url_prefix;
    this.dataSource = new ProjectsDataSource(
      this.projectAppService,
      this.messageService,
    );
    this.policyApiService.getAllUniversities().subscribe({
      next: (response) => {
        this.universities = response;
      },
    });
    this.policyApiService.getAllInstitutes().subscribe({
      next: (response) => {
        this.institutes = response;
      },
    });
    this.policyApiService.getAllFundings().subscribe({
      next: (response) => {
        this.fundings = response;
      },
    });
  }

  save_content_log = '';

  saveContent(): void {
    this.projectAppService.create(this.project).subscribe({
      next: (res) => {
        const projectId = res.url.match(/\d+$/)?.[0]
        console.log(res.url.match(/\d+$/)?.[0]);
        this.accountDataService.get_user_id().subscribe(
          (response: any) => {
            const userId = response;
            console.log(userId);
            this.projectAppService.post_userid(projectId, userId).subscribe(
              (response: any) => {
                console.log('Project created succesfully:', response);
                this.save_content_log = "Project created succesfully"
                this.router.navigateByUrl(`projects/${projectId}`);
              },
              (error) => console.error('Log::', error),
            );
            this.projectAppService.post_policy(projectId, this.selectedUniversityId, this.selectedInstituteId, this.selectedFundingId).subscribe(console.log)
          },
          
          (error) => console.error('Log:', error),
        );//Ende Funktion
      },//Ende res
      error: (e) => {
        console.error(e);
        console.log("Please fill in all fields to create a project")
        this.save_content_log = "Please fill in all fields to create a project"
        return; 
      },


    });
    }
  }

  export const MOCK_DATA = {
    funding: ['Option 1', 'Option 2', 'Option 3'],
    uni: ['Option 1', 'Option 2', 'Option 3'],
    institution: ['Option 1', 'Option 2', 'Option 3'],
    domain: ['Option 1', 'Option 2', 'Option 3']
  };
