import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectAppService } from '../../services/project-app.service';
import { ProjectsDataSource } from '../projects-material-table/projects-material-table.component';
import { MessageService } from '../../message.service';
import { environment } from '@env';
import { AccountDataService } from '@services/account-data.service';

interface Project {
  title: string; //
  short_title: string; //
  displayName: string; //
  description: string; //
  principleInvestigators: string[]; // Array of strings //
  start_date: string; //
  endDate: string; //
  disciplines: string[]; // Array of strings //
  organizations: string[]; // Array of strings //
  keywords: string; //
  visibility: string; //
  grantid: string; //
  //
  projectName: string; //
  catalog: number; //
  user_or_email: string;
  role: string;
  status: string;
}

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.css'],
})
export class UpdateProjectComponent implements OnInit {
  project: Project = {
    title: '',
    short_title: '',
    start_date: '',
    endDate: '',
    description: '',
    displayName: '',
    keywords: '',
    projectName: '',
    principleInvestigators: [],
    disciplines: [],
    organizations: [],
    visibility: '',
    grantid: '',
    user_or_email: '',
    catalog: 0,
    role: '',
    status: ''
  };

  savedContent: Project = { ...this.project };
  frontend_url: string;
  backend_url: string;
  dataSource!: ProjectsDataSource;

  constructor(
    private http: HttpClient,
    private projectAppService: ProjectAppService,
    private messageService: MessageService,
    private accountDataService : AccountDataService) {}

  ngOnInit() {
    // this.fetchProjectData();
    this.backend_url = environment.backend_url_prefix;
    this.frontend_url = environment.frontend_url_prefix;
    this.dataSource = new ProjectsDataSource(
      this.projectAppService,
      this.messageService,
    );
  }

  save_content_log: string = '';

  // fetchProjectData() {
  //   const projectId = res.url.match(/\d+$/)?.[0]
  //   const apiUrl = `${this.backend_url}/projects/${projectId}/`;

  //   this.http.get<Project>(apiUrl).subscribe(
  //     (data: Project) => {
  //       this.project = data;
  //       this.savedContent = { ...this.project };
  //     },
  //     (error) => {
  //       console.error('Error fetching project data:', error);
  //     },
  //   );
  // }

  saveContent(): void {
    this.projectAppService.create(this.project).subscribe({
      next: (res) => {
        const projectId = res.url.match(/\d+$/)?.[0]
        console.log(res.url.match(/\d+$/)?.[0]);
        this.accountDataService.get_user_id().subscribe(
          (response: any) => {
            const userId = response;
            console.log(userId);
            this.projectAppService.post_userid(projectId, userId).subscribe(
              (response: any) => {
                console.log('Project created succesfully:', response);
                this.save_content_log = "Project created succesfully"
              },
              (error) => console.error('Log::', error),
            );
          },
          
          (error) => console.error('Log:', error),
        );
      },
      error: (e) => {
        console.error(e);
        console.log("Please fill in all fields to create a project")
        this.save_content_log = "Please fill in all fields to create a project"
        return; 
      },
    });

  

    }

  onSubmit() {
    console.log('Submitted Project:', this.project);
  }
}
