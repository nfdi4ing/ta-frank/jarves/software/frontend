import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilizationAgreementComponent } from './utilization-agreement.component';

describe('UtilizationAgreementComponent', () => {
  let component: UtilizationAgreementComponent;
  let fixture: ComponentFixture<UtilizationAgreementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilizationAgreementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UtilizationAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
