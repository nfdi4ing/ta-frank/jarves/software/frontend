import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'; // we also need angular router for Nebular to function properly
import {
  NbSidebarModule,
  NbLayoutModule,
  NbButtonModule,
  NbThemeService,
  NbTreeGridModule,
  NbCardModule,
  NbThemeModule,
} from '@nebular/theme';

import { PProjectOverviewRoutingModule } from './p-project-overview-routing.module';
import { CProjectsTableComponent } from './c-projects-table/c-projects-table.component';
import { NbIconModule } from '@nebular/theme';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [CProjectsTableComponent],
  imports: [
    CommonModule,
    RouterModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbLayoutModule,
    NbSidebarModule, // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    NbTreeGridModule,
    NbCardModule,
    NbThemeModule,
    PProjectOverviewRoutingModule,
    NbIconModule,
    MatTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
  ],
  providers: [NbThemeService],
})
export class PProjectOverviewModule {}
