import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CProjectsTableComponent } from './c-projects-table/c-projects-table.component';

const routes: Routes = [
  { path: 'project-overview', component: CProjectsTableComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PProjectOverviewRoutingModule {}
