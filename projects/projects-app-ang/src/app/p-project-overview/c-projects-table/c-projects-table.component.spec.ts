import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CProjectsTableComponent } from './c-projects-table.component';

describe('CProjectsTableComponent', () => {
  let component: CProjectsTableComponent;
  let fixture: ComponentFixture<CProjectsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CProjectsTableComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CProjectsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
