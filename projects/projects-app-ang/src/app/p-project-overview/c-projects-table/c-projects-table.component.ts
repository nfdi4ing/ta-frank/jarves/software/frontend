import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  ElementRef,
} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';

import { BehaviorSubject, Observable, of, merge, fromEvent } from 'rxjs';
import {
  catchError,
  finalize,
  tap,
  debounceTime,
  distinctUntilChanged,
} from 'rxjs/operators';

import { ProjectAppService } from '../../services/project-app.service';
import { Project } from '../../Project';
import { MessageService } from '../../message.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-c-projects-table',
  templateUrl: './c-projects-table.component.html',
  styleUrls: ['./c-projects-table.component.css'],
})
export class CProjectsTableComponent implements OnInit, AfterViewInit {
  project!: Project[];
  dataSource!: ProjectsDataSource;

  // Paginator
  displayedColumns = ['id', 'title', 'status', 'description'];
  pageSizeOptions = [3, 5, 10, 100];
  pageIndex: number;
  pageSize: number;
  length: number;

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('input') input!: ElementRef;

  constructor(
    private projectAppService: ProjectAppService,
    private messageService: MessageService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.project = this.route.snapshot.data['project'];
    this.dataSource = new ProjectsDataSource(
      this.projectAppService,
      this.messageService,
    );
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.getAllProjects()
    this.dataSource.loadProjects('', 'asc', 0, 3);
    console.log('ngOnInit()');
    console.log(this.dataSource);
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadProjectsPage();
        }),
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadProjectsPage()))
      .subscribe();
  }

  loadProjectsPage() {
    //this.dataSource.getAllProjects()

    this.dataSource.loadProjects(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }

  getProject(row = '') {
    console.log('getProject');
    console.log(row);
  }
}

export class ProjectsDataSource implements DataSource<Project> {
  private projectsSubject = new BehaviorSubject<Project[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  projects: Project[] = [];

  public loading$ = this.loadingSubject.asObservable();

  constructor(
    private projectService: ProjectAppService,
    private messageService: MessageService,
  ) {}

  connect(): Observable<Project[]> {
    return this.projectsSubject.asObservable();
  }

  disconnect(): void {
    this.projectsSubject.complete();
    this.loadingSubject.complete();
  }

  loadProjects(
    filter = '',
    sortDirection = 'asc',
    pageIndex = 0,
    pageSize: number,
  ) {
    this.loadingSubject.next(true);

    this.projectService
      .findProjects(filter, sortDirection, pageIndex, pageSize)
      .pipe(
        catchError(
          (error: { message: string }) => {
            this.log('Caught in CatchError. Returning []');
            console.log('loadProjects.pipe');
            console.error(error); // log to console instead
            return of([]);
          },
          //() => of([])
        ),
        finalize(() => this.loadingSubject.next(false)),
        tap(() => this.log(`projects-material-table.components.ts: finalize`)),
      );
    /* .subscribe(
            project => this.projectsSubject.next(project), */
  }

  getAllProjects(): void {
    this.projectService
      .getAll()
      .subscribe((project) => this.projectsSubject.next(project));
  }

  /** Log a MessageService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`MessageService: ${message}`);
  }
}
