// page.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PageService {
  private isLandingPage = false;
  private isMainPage = true;

  // constructor() {}

  setIsLandingPage(isLandingPage: boolean): void {
    this.isLandingPage = isLandingPage;
  }

  getIsLandingPage(): boolean {
    return this.isLandingPage;
  }

  setIsMainPage(isMainPage: boolean): void {
    this.isMainPage = isMainPage;
  }

  getIsMainPage(): boolean {
    return this.isMainPage;
  }
}
