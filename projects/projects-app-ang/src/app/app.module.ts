import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms'; /** Allows to implement forms to fill out by users */
import { HttpClient, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http'; /** Allows communication via API to the Django backend */

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/** Import Components */
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { MessagesComponent } from './messages/messages.component';
import { MHeaderComponent } from './components/main/m-header/m-header.component';
import { CHomeComponent } from './components/main/m-header/c-home/c-home.component';
import { CBarComponent } from './components/main/m-header/c-bar/c-bar.component';
import { CLanguageComponent } from './components/main/m-header/c-language/c-language.component';
import { CAccountComponent } from './components/main/m-header/c-account/c-account.component';
import { CExitComponent } from './components/main/m-header/c-exit/c-exit.component';
import { MainComponent } from './components/main/main.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgxFileDropModule } from 'ngx-file-drop';



/** Material Modules */
import { MatTableModule } from '@angular/material/table';
import { MatInputModule} from "@angular/material/input"
import { MatPaginatorModule} from "@angular/material/paginator"
import { MatSortModule} from "@angular/material/sort"
import { MatProgressSpinnerModule} from "@angular/material/progress-spinner"
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { ProjectsMaterialTableComponent } from './components/projects-material-table/projects-material-table.component'

/** Import Modules */
import { PProjectOverviewModule } from './p-project-overview/p-project-overview.module';
import { NbCheckboxModule, NbToastrModule } from '@nebular/theme';
import { NbFormFieldComponent, NbSidebarModule, NbThemeModule} from '@nebular/theme';
import { NbThemeService, NbActionsModule, NbLayoutModule } from '@nebular/theme';
import { NbPopoverModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbSidebarService } from '@nebular/theme';
import { NbButtonModule,NbCardModule,NbIconModule,NbSelectModule,NbFormFieldModule,NbStepperModule,NbAccordionModule,NbInputModule,NbTreeGridModule,NbListModule } from '@nebular/theme';
import { NbToggleModule } from '@nebular/theme';
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";

//Auth
import { NbPasswordAuthStrategy, NbAuthModule, NbOAuth2AuthStrategy, NbAuthJWTToken, NbLoginComponent } from '@nebular/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { AuthInterceptor } from './services/auth.interceptor';

import {A11yModule} from '@angular/cdk/a11y';
import { CLandingComponent } from './components/main/m-header/c-landing/c-landing.component';
import { LandingComponent } from './components/landing/landing.component';
import { NgxLoginComponent } from './components/auth/login/login.component';
import { Table1Component } from './components/project-details/table1/table1.component';
import { GuidelinesComponent } from './components/guidelines/guidelines.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AccountComponent } from './components/account/account.component';
import { ChecklisteComponent } from './components/project-details/checkliste/checkliste.component';
import { TrainingsComponent } from './components/project-details/trainings/trainings.component';
import { UpdateProjectComponent } from './components/update-project/update-project.component';
import { ChatComponent } from './components/project-details/chat/chat.component';
import { InviteComponent } from './components/project-details/invite/invite.component';
import { DateiUploadComponent } from './components/project-details/datei-upload/datei-upload.component';
import { PdfViewerComponent } from './components/project-details/pdf-viewer/pdf-viewer.component';
import { RollenComponent } from './components/steps/rollen/rollen.component';
import { FoerdervorgabenComponent } from './components/steps/foerdervorgaben/foerdervorgaben.component';
import { SkizzeComponent } from './components/steps/skizze/skizze.component';
import { EinleitungComponent } from './components/phases/einleitung/einleitung.component';
import { PlanenComponent } from './components/phases/planen/planen.component';
import { DmpSoftwareComponent } from './components/steps/dmp-software/dmp-software.component';
import { ExisitierendeDatenComponent } from './components/steps/exisitierende-daten/exisitierende-daten.component';
import { DatenverwaltungComponent } from './components/steps/datenverwaltung/datenverwaltung.component';
import { MetadatenComponent } from './components/steps/metadaten/metadaten.component';
import { EUComponent } from './components/steps/e-u/e-u.component';
import { KostenComponent } from './components/steps/kosten/kosten.component';
import { DurchfuehrenComponent } from './components/steps/durchfuehren/durchfuehren.component';
import { SpeichernComponent } from './components/steps/speichern/speichern.component';
import { AufbereitenComponent } from './components/steps/aufbereiten/aufbereiten.component';
import { FremddatenComponent } from './components/steps/fremddaten/fremddaten.component';
import { VorbereitungComponent } from './components/steps/vorbereitung/vorbereitung.component';
import { AnalysierenComponent } from './components/steps/analysieren/analysieren.component';
import { DokumentationComponent } from './components/steps/dokumentation/dokumentation.component';
import { PlanungsphaseComponent } from './components/steps/planungsphase/planungsphase.component';
import { VorbereitenComponent } from './components/steps/vorbereiten/vorbereiten.component';
import { UmsetzenComponent } from './components/steps/umsetzen/umsetzen.component';
import { Speichern2Component } from './components/steps/speichern2/speichern2.component';
import { Vorbereitung2Component } from './components/steps/vorbereitung2/vorbereitung2.component';
import { Analysieren2Component } from './components/steps/analysieren2/analysieren2.component';
import { DatenauswahlComponent } from './components/steps/datenauswahl/datenauswahl.component';
import { SpeichermediumComponent } from './components/steps/speichermedium/speichermedium.component';
import { FormatComponent } from './components/steps/format/format.component';
import { PruefungComponent } from './components/steps/pruefung/pruefung.component';
// import { ArchivierenComponent } from './components/steps/archivieren/archivieren.component';
import { AnalyseComponent } from './components/phases/analyse/analyse.component';
import { ErhebungComponent } from './components/phases/erhebung/erhebung.component';
import { Durchfuehren2Component } from './components/steps/durchfuehren2/durchfuehren2.component';
import { Aufbereiten2Component } from './components/steps/aufbereiten2/aufbereiten2.component';
import { Fremddaten2Component } from './components/steps/fremddaten2/fremddaten2.component';
import { Dokumentation2Component } from './components/steps/dokumentation2/dokumentation2.component';

import { ArchivierenComponent } from './components/phases/archivieren/archivieren.component';
import { ArchivierenStepComponent } from './components/steps/archivieren-step/archivieren-step.component';

import { ImpressumComponent } from './components/impressum/impressum.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { UtilizationAgreementComponent } from './components/utilization-agreement/utilization-agreement.component';

import { CreatePolicyComponent } from './components/create-policy/create-policy.component';

import { ExecutionComponent } from './components/phases/execution/execution.component';
import { PolicyModule } from './policy/policy.module';
import { ProjectPolicyTableComponent } from './components/project-policy-table/project-policy-table.component';
import { FindingPlanComponent } from './components/steps/execution/finding-plan/finding-plan.component';
import { FindingCreateReuseComponent } from './components/steps/execution/finding-create-reuse/finding-create-reuse.component';
import { FindingCompileComponent } from './components/steps/execution/finding-compile/finding-compile.component';
import { ArtifactCheckComponent } from './components/steps/execution/artifact-check/artifact-check.component';
import { ArtifactGenerateComponent } from './components/steps/execution/artifact-generate/artifact-generate.component';
import { ArtifactPlanComponent } from './components/steps/execution/artifact-plan/artifact-plan.component';
import { ArtifactReuseComponent } from './components/steps/execution/artifact-reuse/artifact-reuse.component';
import { ArtifactStoreComponent } from './components/steps/execution/artifact-store/artifact-store.component';


//import { NbLoginComponent } from './components/auth/login/login.component';


// Factory function required during AOT compilation
export function httpTranslateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ProjectsListComponent,
    ProjectDetailsComponent,
    AddProjectComponent,
    MessagesComponent,
    MHeaderComponent,
    CHomeComponent,
    CBarComponent,
    CLanguageComponent,
    CAccountComponent,
    CExitComponent,
    MainComponent,
    ProjectsMaterialTableComponent,
    CLandingComponent,
    LandingComponent,
    Table1Component,
    GuidelinesComponent,
    SettingsComponent, 
    AccountComponent,
    ChecklisteComponent,
    TrainingsComponent,
    UpdateProjectComponent,
    ChatComponent,
    InviteComponent,
    DateiUploadComponent,
    PdfViewerComponent,
    RollenComponent,
    FoerdervorgabenComponent,
    SkizzeComponent,
    EinleitungComponent,
    PlanenComponent,
    DmpSoftwareComponent,
    ExisitierendeDatenComponent,
    DatenverwaltungComponent,
    MetadatenComponent,
    EUComponent,
    KostenComponent,
    DurchfuehrenComponent,
    SpeichernComponent,
    AufbereitenComponent,
    FremddatenComponent,
    VorbereitungComponent,
    AnalysierenComponent,
    DokumentationComponent,
    PlanungsphaseComponent,
    VorbereitenComponent,
    UmsetzenComponent,
    Speichern2Component,
    Vorbereitung2Component,
    Analysieren2Component,
    DatenauswahlComponent,
    SpeichermediumComponent,
    FormatComponent,
    PruefungComponent,
    ArchivierenComponent,
    AnalyseComponent,
    ErhebungComponent,
    Durchfuehren2Component,
    Aufbereiten2Component,
    Fremddaten2Component,
    Dokumentation2Component,
    ArchivierenStepComponent,
    ImpressumComponent,
    PrivacyPolicyComponent,
    UtilizationAgreementComponent,
    CreatePolicyComponent,
    ExecutionComponent,
    ProjectPolicyTableComponent,
    //NbLoginComponent
    //NgxLoginComponent
    FindingPlanComponent,
    FindingCreateReuseComponent,
    FindingCompileComponent,
    ArtifactCheckComponent,
    ArtifactGenerateComponent,
    ArtifactPlanComponent,
    ArtifactReuseComponent,
    ArtifactStoreComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    /** Add Our modules */
    PProjectOverviewModule,
    FormsModule, /** Add the forms module */
    HttpClientModule, /** Add the HttpClient module */
    HttpClientXsrfModule.withOptions({  // Jonas's POST csrf fix
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken', // 'HTTP_X_CSRFTOKEN',
    }),
    NgbModule,

    /** Add Material modules */
    MatTableModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    CommonModule,
    NgxFileDropModule,

    /** nebular modules */
    NbSidebarModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' }),
    NbEvaIconsModule,
    NbActionsModule,
    NbLayoutModule,
    NbToggleModule,
    NbEvaIconsModule,
    NbInputModule,
    NbTreeGridModule,
    NbCheckboxModule,
    Ng2SmartTableModule,
    NbPopoverModule,
    NgbTooltipModule,
    A11yModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoaderFactory,
        deps: [HttpClient]
      }
    }),

    NbButtonModule,
    NbListModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbFormFieldModule,
    NbStepperModule,
    NbAccordionModule,
    NbToastrModule.forRoot(),
    NbAuthModule.forRoot({
      strategies: [
        NbOAuth2AuthStrategy.setup({
        name: 'oauth',

        redirect: {
          success: '/welcome/',
          failure: null as any, // stay on the same page
        },
        clientId: ''
      }),
        NbPasswordAuthStrategy.setup({
          name: 'email',

          token: {
            class: NbAuthJWTToken,
            key: 'token', // this parameter tells where to look for the token
          },

          baseEndpoint: 'http://example.com/app-api/v1',
            login: {
              redirect: {
                success: '/projects/',
                failure: null, // stay on the same page
              },
              endpoint: '/auth/sign-in',
              method: 'post',
            },
            register: {
              redirect: {
                success: '/landing/',
                failure: null, // stay on the same page
              },
              endpoint: '/auth/sign-up',
              method: 'post',
            },
            logout: {
              endpoint: '/auth/sign-out',
              method: 'post',
            },
            requestPass: {
              endpoint: '/auth/request-pass',
              method: 'post',
            },
            resetPass: {
              endpoint: '/auth/reset-pass',
              method: 'post',
            },
        }),
      ],

      forms: {
        login: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          },
        },
        register: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          },
        },
        requestPassword: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          },
        },
        resetPassword: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          },
        },
        logout: {
          redirectDelay: 0,
        },
      },
    }),
    PolicyModule
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
