export interface Account {

    api_key?: string;
    dark_mode?: boolean;

    id?: number;
    title?: string;
    short_title?: string;
    status?: string;
    start_date?: string;
    description?: string;

}
