export interface NeedsForHelp {
  number?: number;
  datetime?: string;
}
