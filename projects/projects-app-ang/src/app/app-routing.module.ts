import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**Import Components*/
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { CProjectsTableComponent } from './p-project-overview/c-projects-table/c-projects-table.component';

/**Import Modules*/
import { PProjectOverviewModule } from './p-project-overview/p-project-overview.module';
import { PProjectOverviewRoutingModule } from './p-project-overview/p-project-overview-routing.module';
import { LandingComponent } from './components/landing/landing.component';
import { AuthComponent } from './components/auth/auth.component';
import { AccountComponent } from './components/account/account.component';
//import { NbLoginComponent } from '@nebular/auth';
//import { ProjectDetailsComponent } from './components/project-details/project-details.component';

export const routes: Routes = [
  // ...
  { path: '', component: LandingComponent}, // ,children: [
    //{ path: 'login', component: NbLoginComponent }, // Content for the landing page
    // Add other routes for your application here
  //] },
  //{ path: '', redirectTo: 'projects', pathMatch: 'full'},
  { path: 'projects', component: ProjectsListComponent},
  { path: 'projects/:id', component: ProjectDetailsComponent},
  { path: 'add', component:AddProjectComponent},
  { path: 'project-overview', component:CProjectsTableComponent},
  { path: 'landing', component: LandingComponent},
  { path: 'guidelines', component: GuidelinesComponent},
  { path: 'settings', component: SettingsComponent},
  { path: 'account', component: AccountComponent},
  { path: 'impressum', component: ImpressumComponent},
  { path: 'privacy_policy', component: PrivacyPolicyComponent},
  { path: 'utilization_agreement', component: UtilizationAgreementComponent},
  //{ path: 'project-details/:id', component: ProjectDetailsComponent },
  { path: 'auth',
   loadChildren: () => import('./components/auth/auth.module').then(m => m.NgxAuthModule) },
   { path: 'projectsettings', component: UpdateProjectComponent},
   { path: 'create_policy', component: CreatePolicyComponent},
   { path: 'policies', loadChildren: () => import('./policy/policy.module').then(m => m.PolicyModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes),PProjectOverviewRoutingModule,],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import {
  NbAuthComponent,
  NbLoginComponent,
  NbRegisterComponent,
  NbLogoutComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import { GuidelinesComponent } from './components/guidelines/guidelines.component';
import { SettingsComponent } from './components/settings/settings.component';
import { UpdateProjectComponent } from './components/update-project/update-project.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { UtilizationAgreementComponent } from './components/utilization-agreement/utilization-agreement.component';
import { CreatePolicyComponent } from './components/create-policy/create-policy.component';


