import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Funding } from '@services/Interfaces/Funding';
import { PolicyApiService } from '@services/policy-api.service';

@Component({
  selector: 'policy-list-funding',
  templateUrl: './list-funding.component.html',
  styleUrls: ['./list-funding.component.css'],
})
export class ListFundingComponent implements OnInit{
  fundings: Funding[] = [];
  // displayedColumns = ['id', 'name', 'acronym', 'actions'];
  displayedColumns = ['id', 'name', 'acronym'];
  pageSizeOptions = [3, 5, 10, 100];

  constructor(
    private policyApiService: PolicyApiService,
    private cdr: ChangeDetectorRef,
  ) {}
  
  ngOnInit(): void {
    this.policyApiService.getAllFundings().subscribe({
      next: (universities) => {
        this.fundings = universities;
        this.cdr.detectChanges();
      },
    });
  }
  
}
