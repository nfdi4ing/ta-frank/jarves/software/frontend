import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { PolicyApiService } from '@services/policy-api.service';
import { TranslateService } from '@ngx-translate/core';
import { Policy } from '@services/Interfaces/Policy';

@Component({
  selector: 'policy-add-edit-funding',
  templateUrl: './add-edit-funding.component.html',
  styleUrls: ['./add-edit-funding.component.css'],
})
export class AddEditFundingComponent implements OnInit {
  addMode = true;
  id = 0;
  form: FormGroup;
  loading = false;
  policies: Policy[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private policyApiService: PolicyApiService,
    private toastrService: NbToastrService,
    private router: Router,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = parseInt(id, 10);
      this.addMode = false;
    }
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      acronym: [''],
      policy: ['', Validators.required],
    });

    this.policyApiService.getAllPolicies().subscribe({
      next: (response) => {
        this.policies = response;
        this.cdr.detectChanges();
      },
    });

    if (!this.addMode) {
      this.policyApiService.getUniversity(this.id).subscribe({
        next: (response) => {
          this.form.patchValue(response);
        },
        error: (error) => {
          const errorMessage: string = this.translate.instant(
            'POLICY.MESSAGES.FUNDING_NOT_FOUND',
          );
          this.toastrService.danger(errorMessage);
        },
      });
    }
  }
  get f() {
    return this.form.controls;
  }
  isDisabled() {
    return this.form.invalid || this.loading;
  }

  goBack() {
    window.history.back();
  }
  addUniversity() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.policyApiService.createFunding(this.form.value).subscribe({
      next: (response) => {
        const successMessage: string = this.translate.instant(
          'POLICY.MESSAGES.FUNDING_ADDED',
        );
        this.toastrService.success(successMessage);
        this.router.navigate(['/policies/funding']);
      },
      error: (error) => {
        const errorMessage: string = this.translate.instant(
          'POLICY.MESSAGES.GENERIC_ERROR',
        );
        this.loading = false;
        this.toastrService.danger(errorMessage);
      },
    });
  }
}
