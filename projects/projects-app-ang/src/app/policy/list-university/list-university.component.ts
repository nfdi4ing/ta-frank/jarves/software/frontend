import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { University } from '@services/Interfaces/University';
import { PolicyApiService } from '@services/policy-api.service';

@Component({
  selector: 'policy-list-university',
  templateUrl: './list-university.component.html',
  styleUrls: ['./list-university.component.css'],
})
export class ListUniversityComponent implements OnInit{
  universities: University[] = [];
  // displayedColumns = ['id', 'name', 'acronym', 'actions'];
  displayedColumns = ['id', 'name', 'acronym'];
  pageSizeOptions = [3, 5, 10, 100];

  constructor(
    private policyApiService: PolicyApiService,
    private cdr: ChangeDetectorRef,
  ) {}
  
  ngOnInit(): void {
    this.policyApiService.getAllUniversities().subscribe({
      next: (universities) => {
        this.universities = universities;
        this.cdr.detectChanges();
      },
    });
  }
  
}
