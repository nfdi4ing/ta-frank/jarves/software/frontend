import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { Policy } from '@services/Interfaces/Policy';
import { University } from '@services/Interfaces/University';
import { PolicyApiService } from '@services/policy-api.service';

@Component({
  selector: 'policy-add-edit-institute',
  templateUrl: './add-edit-institute.component.html',
  styleUrls: ['./add-edit-institute.component.css'],
})
export class AddEditInstituteComponent implements OnInit{
  addMode = true;
  id = 0;
  form: FormGroup;
  universities: University[] = [];
  policies: Policy[] = [];
  loading = false;

  constructor(
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute,
    private policyApiService: PolicyApiService,
    private toastrService: NbToastrService,
    private router: Router,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef
    ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = parseInt(id, 10);
      this.addMode = false;
    }
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      acronym: [''],
      university: ['', Validators.required],
      policy: ['', Validators.required],
    })

    this.policyApiService.getAllUniversities().subscribe({
      next: (universities) => {
        this.universities = universities;
        this.cdr.detectChanges();
      }
    });
    this.policyApiService.getAllPolicies().subscribe({
      next: (response) => {
        this.policies = response;
        this.cdr.detectChanges();
      },
    });

    if(!this.addMode) {
      this.policyApiService.getInstitute(this.id).subscribe({
        next: (response) => {
          this.form.patchValue(response);
        },
        error: (error) => {
          const errorMessage:string = this.translate.instant("POLICY.MESSAGES.INSTITUTE_NOT_FOUND");
          this.toastrService.danger(errorMessage);
        },
      });
    }
  }
  get f() { return this.form.controls; }

  isDisabled() {
    return this.form.invalid || this.loading;
  }

  goBack() {
    console.log('go back');
    window.history.back();
  }
  addUniversity() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.policyApiService
      .createInstitute(this.form.value)
      .subscribe({
        next: (response) => {
          const successMessage:string = this.translate.instant("POLICY.MESSAGES.INSTITUTE_ADDED");
          this.toastrService.success(successMessage);
          this.router.navigate(['/policies/institute']);
        },
        error: (error) => {
          const errorMessage:string = this.translate.instant("POLICY.MESSAGES.GENERIC_ERROR");
          this.loading = false;
          this.toastrService.danger(errorMessage);
        },
      });
  }
  
}
