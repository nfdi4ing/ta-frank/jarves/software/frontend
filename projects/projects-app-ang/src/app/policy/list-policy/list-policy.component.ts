import { CommonModule } from "@angular/common";
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Policy, PolicyKind } from "@services/Interfaces/Policy";
import { PolicyApiService } from "@services/policy-api.service";
import { forkJoin, Observable } from "rxjs";

@Component({
  selector: 'policy-list-policy',
  templateUrl: './list-policy.component.html',
  styleUrls: ['./list-policy.component.css'],
})
export class ListPolicyComponent implements OnInit {
  policies: Policy[] = [];
  policyKinds: PolicyKind[] = [];
  // displayedColumns = ['id', 'name', 'kind', 'actions'];
  displayedColumns = ['id', 'name', 'kind'];
  pageSizeOptions = [3, 5, 10, 100];

  constructor(
    private policyApiService: PolicyApiService,
    private cdr: ChangeDetectorRef,
  ) {}
  
  ngOnInit(): void {
    forkJoin([this.policyApiService.getAllPolicyKinds(), this.policyApiService.getAllPolicies()]).subscribe(([kinds, policies]) => {
      policies.forEach(policy => {
        const kind = kinds.filter(x => x.id == policy.kind)
        if(kind && kind.length > 0) policy.kind = kind[0]
      })
      this.policies = policies
      this.cdr.detectChanges()
    })

  }
}
