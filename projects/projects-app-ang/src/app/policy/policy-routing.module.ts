import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddEditInstituteComponent } from "./add-edit-institute/add-edit-institute.component";
import { AddEditPolicyComponent } from "./add-edit-policy/add-edit-policy.component";
import { AddEditUniversityComponent } from "./add-edit-university/add-edit-university.component";
import { ListInstituteComponent } from "./list-institute/list-institute.component";
import { ListPolicyComponent } from "./list-policy/list-policy.component";
import { ListUniversityComponent } from "./list-university/list-university.component";
import { ListFundingComponent } from "./list-funding/list-funding.component";
import { AddEditFundingComponent } from "./add-edit-funding/add-edit-funding.component";

export const routes: Routes = [
    { path: 'university', component: ListUniversityComponent},
    { path: 'university/add', component: AddEditUniversityComponent},
    { path: 'university/edit/:id', component: AddEditUniversityComponent},
    { path: 'institute', component: ListInstituteComponent},
    { path: 'institute/add', component: AddEditInstituteComponent},
    { path: 'institute/edit/:id', component: AddEditInstituteComponent},
    { path: 'policy', component: ListPolicyComponent},
    { path: 'policy/add', component: AddEditPolicyComponent},
    { path: 'policy/edit/:id', component: AddEditPolicyComponent},
    { path: 'funding', component: ListFundingComponent},
    { path: 'funding/add', component: AddEditFundingComponent},
    { path: 'funding/edit/:id', component: AddEditFundingComponent},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PolicyRoutingModule { }