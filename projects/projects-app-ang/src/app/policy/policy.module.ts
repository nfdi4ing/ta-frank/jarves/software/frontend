import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import { AddEditInstituteComponent } from './add-edit-institute/add-edit-institute.component';
import { AddEditUniversityComponent } from './add-edit-university/add-edit-university.component';
import { ListInstituteComponent } from './list-institute/list-institute.component';
import { PolicyRoutingModule } from './policy-routing.module';
import { ListUniversityComponent } from './list-university/list-university.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { ListPolicyComponent } from './list-policy/list-policy.component';
import { AddEditPolicyComponent } from './add-edit-policy/add-edit-policy.component';
import { ListFundingComponent } from './list-funding/list-funding.component';
import { AddEditFundingComponent } from './add-edit-funding/add-edit-funding.component';

export function httpTranslateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    ListUniversityComponent,
    ListInstituteComponent,
    ListFundingComponent,
    ListPolicyComponent,
    AddEditUniversityComponent,
    AddEditInstituteComponent,
    AddEditPolicyComponent,
    AddEditFundingComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PolicyRoutingModule,

    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoaderFactory,
        deps: [HttpClient],
      },
    }),

    NbIconModule,
    NbCardModule,
    MatTableModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbAccordionModule,
  ],
})
export class PolicyModule {}
