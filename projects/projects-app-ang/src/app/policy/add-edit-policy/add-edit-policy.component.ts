import { CommonModule } from "@angular/common";
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { TranslateService } from "@ngx-translate/core";
import { DmpTool, PolicyKind, StorageTool } from "@services/Interfaces/Policy";
import { PolicyApiService } from "@services/policy-api.service";

@Component({
  selector: 'policy-add-edit-policy',
  templateUrl: './add-edit-policy.component.html',
  styleUrls: ['./add-edit-policy.component.css'],
})
export class AddEditPolicyComponent {
  addMode = true;
  id = 0;
  form: FormGroup;
  loading = false;
  policyKinds: PolicyKind[] = []
  dmpTools: DmpTool[] = []
  storageTools: StorageTool[] = []
  scores = [
    1, 2, 3, 4, 5
  ]

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private policyApiService: PolicyApiService,
    private toastrService: NbToastrService,
    private router: Router,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {
    this.loadAllData()
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = parseInt(id, 10);
      this.addMode = false;
    }
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      kind: ['', Validators.required],
      dmp_recommendation: [''],
      storage_recommendation: [''],
      score1: [''],
      score2: ['']
    });

    if (!this.addMode) {
      this.policyApiService.getUniversity(this.id).subscribe({
        next: (response) => {
          this.form.patchValue(response);
        },
        error: (error) => {
          const errorMessage:string = this.translate.instant("POLICY.MESSAGES.UNIVERSITY_NOT_FOUND")
          this.toastrService.danger(errorMessage);
        },
      });
    }
  }

  loadAllData() {
    this.policyApiService.getAllDmpTools().subscribe({
      next: (response) => {
        this.dmpTools = response
        this.cdr.detectChanges()
      }
    })
    this.policyApiService.getAllPolicyKinds().subscribe({
      next: (response) => {
        this.policyKinds = response
        this.cdr.detectChanges()
      }
    })
    this.policyApiService.getAllStorageTools().subscribe({
      next: (response) => {
        this.storageTools = response
        this.cdr.detectChanges()
      }
    })
  }
  
  get f() {
    return this.form.controls;
  }
  isDisabled() {
    return this.form.invalid || this.loading;
  }

  goBack() {
    window.history.back();
  }
  addUniversity() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.policyApiService
      .createPolicy(this.form.value)
      .subscribe({
        next: (response) => {
          const successMessage:string = this.translate.instant("POLICY.MESSAGES.POLICY_ADDED")
          this.toastrService.success(successMessage);
          this.router.navigate(['/policies/policy']);
        },
        error: (error) => {
          const errorMessage:string = this.translate.instant("POLICY.MESSAGES.GENERIC_ERROR")
          this.loading = false;
          this.toastrService.danger(errorMessage);
        },
      });
  }
}
