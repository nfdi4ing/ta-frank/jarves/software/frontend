import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Institute } from '@services/Interfaces/Institute';
import { PolicyApiService } from '@services/policy-api.service';

@Component({
  selector: 'policy-list-institute',
  templateUrl: './list-institute.component.html',
  styleUrls: ['./list-institute.component.css'],
})
export class ListInstituteComponent implements OnInit{
  institutes: Institute[] = [];
  // displayedColumns = ['id', 'name', 'acronym', 'actions'];
  displayedColumns = ['id', 'name', 'acronym'];
  pageSizeOptions = [3, 5, 10, 100];
  constructor(
    private policyApiService: PolicyApiService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.policyApiService.getAllInstitutes().subscribe({
      next: (institutes) => {
        this.institutes = institutes;
        this.cdr.detectChanges();
      },
    });
  }
}
