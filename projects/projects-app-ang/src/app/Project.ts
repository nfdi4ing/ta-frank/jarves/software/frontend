export interface Project {
  id?: number;
  title?: string;
  short_title?: string;
  status?: string;
  start_date?: string;
  description?: string;
}
