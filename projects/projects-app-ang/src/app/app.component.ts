import { Component, ChangeDetectionStrategy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NbSidebarService } from '@nebular/theme';
import { PageService } from './page.service';
import { AuthComponent } from './components/auth/auth.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'ProjectsAppAng';

  constructor(
    public translate: TranslateService,
    private sidebarService: NbSidebarService,
    private pageService: PageService
  ){ 
      // Register translation languages
      translate.addLangs(['🇬🇧', '🇩🇪']);
      // Set default language
      translate.setDefaultLang('🇬🇧');
    } 

    get isLandingPage(): boolean {
      return this.pageService.getIsLandingPage();}
    
    get isMainPage(): boolean {
        return this.pageService.getIsMainPage();}
    //Switch language

    translateLanguageTo(lang: string) { 
      this.translate.use(lang);
  }

  toggle() {
    this.sidebarService.toggle(true, 'left');
  }
}
