# Allgemein 

Jarves führt Forscher mithilfe eines geführten Prozesses durch das Forschungsdatenmanagement, vermittelt Wissen und Werkzeuge zur Entscheidungsfindung im FDM. Schaue dir die Demo an auf: Jarves.nfdi4ing.de. Die aktive Entwicklung findet in einem anderen Repository statt und eine Unterstützung oder Anpassung an eigene Anforderungen ist dort möglich. Wenden Sie sich bitte an den aktuellen Kontakt in: https://jarves.nfdi4ing.de/impressum.

# Funding 

Die Software stammt aus dem NFDI4Ing-Projekt. NFDI4Ing wird von der DFG unter der Projekt-Nr. 442146713 gefördert.