'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">angular-frontend documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-9f3b32df15a4657fec6b3fb3271f81500ad70fc18c07c263f4e009f372a18f058acf78aa9729ae4f18a422be9beed2f501d82a4fbcfd9dfb6229aba970717769"' : 'data-target="#xs-components-links-module-AppModule-9f3b32df15a4657fec6b3fb3271f81500ad70fc18c07c263f4e009f372a18f058acf78aa9729ae4f18a422be9beed2f501d82a4fbcfd9dfb6229aba970717769"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-9f3b32df15a4657fec6b3fb3271f81500ad70fc18c07c263f4e009f372a18f058acf78aa9729ae4f18a422be9beed2f501d82a4fbcfd9dfb6229aba970717769"' :
                                            'id="xs-components-links-module-AppModule-9f3b32df15a4657fec6b3fb3271f81500ad70fc18c07c263f4e009f372a18f058acf78aa9729ae4f18a422be9beed2f501d82a4fbcfd9dfb6229aba970717769"' }>
                                            <li class="link">
                                                <a href="components/AddProjectComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddProjectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CAbschlussComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CAbschlussComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CAccountComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CAccountComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CAnalyseComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CAnalyseComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CArchivierungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CArchivierungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CBarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CBarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CDurchfuehrungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CDurchfuehrungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CErhebungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CErhebungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CExitComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CExitComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CHomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CHomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CLandingComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CLandingComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CLanguageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CLanguageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CNachzutzungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CNachzutzungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CPlanungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CPlanungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CVorbereitungComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CVorbereitungComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CZugangComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CZugangComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LandingComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LandingComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MHeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MHeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MainComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MainComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessagesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MessagesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MyProjectComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MyProjectComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProjectDetailsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProjectDetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProjectsListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProjectsListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProjectsMaterialTableComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProjectsMaterialTableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NgxAuthModule.html" data-type="entity-link" >NgxAuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NgxAuthModule-76999ec174c7399d84bb63243df3612e9858fdef8735850ce01679ae6ee30254d00ece42553f2021dc00bca7b3b3e3de501dfebcd7e8ccd355ffae15ca5c4e87"' : 'data-target="#xs-components-links-module-NgxAuthModule-76999ec174c7399d84bb63243df3612e9858fdef8735850ce01679ae6ee30254d00ece42553f2021dc00bca7b3b3e3de501dfebcd7e8ccd355ffae15ca5c4e87"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NgxAuthModule-76999ec174c7399d84bb63243df3612e9858fdef8735850ce01679ae6ee30254d00ece42553f2021dc00bca7b3b3e3de501dfebcd7e8ccd355ffae15ca5c4e87"' :
                                            'id="xs-components-links-module-NgxAuthModule-76999ec174c7399d84bb63243df3612e9858fdef8735850ce01679ae6ee30254d00ece42553f2021dc00bca7b3b3e3de501dfebcd7e8ccd355ffae15ca5c4e87"' }>
                                            <li class="link">
                                                <a href="components/LogoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LogoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NgxLoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NgxLoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RequestPasswordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RequestPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ResetPasswordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ResetPasswordComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NgxAuthRoutingModule.html" data-type="entity-link" >NgxAuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PProjectOverviewModule.html" data-type="entity-link" >PProjectOverviewModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PProjectOverviewModule-0d67b35280b6e05c7be4e24e414b50c811b5c1567194100a6b398ef460acadc602d881567c13bdad7dd897753cef06520ec0476850b3c353d89bec14bc110148"' : 'data-target="#xs-components-links-module-PProjectOverviewModule-0d67b35280b6e05c7be4e24e414b50c811b5c1567194100a6b398ef460acadc602d881567c13bdad7dd897753cef06520ec0476850b3c353d89bec14bc110148"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PProjectOverviewModule-0d67b35280b6e05c7be4e24e414b50c811b5c1567194100a6b398ef460acadc602d881567c13bdad7dd897753cef06520ec0476850b3c353d89bec14bc110148"' :
                                            'id="xs-components-links-module-PProjectOverviewModule-0d67b35280b6e05c7be4e24e414b50c811b5c1567194100a6b398ef460acadc602d881567c13bdad7dd897753cef06520ec0476850b3c353d89bec14bc110148"' }>
                                            <li class="link">
                                                <a href="components/CProjectsTableComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CProjectsTableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PProjectOverviewRoutingModule.html" data-type="entity-link" >PProjectOverviewRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/AuthComponent.html" data-type="entity-link" >AuthComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/ProjectsDataSource.html" data-type="entity-link" >ProjectsDataSource</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProjectsDataSource-1.html" data-type="entity-link" >ProjectsDataSource</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/MessageService.html" data-type="entity-link" >MessageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PageService.html" data-type="entity-link" >PageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProjectAppService.html" data-type="entity-link" >ProjectAppService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Project.html" data-type="entity-link" >Project</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});