# Use the official Node.js 14 image as the base image
FROM node:14 AS builder
# Set the working directory to /app
WORKDIR /app

COPY package*.json ./

RUN npm install
RUN npm uninstall typescript
RUN npm install typescript@">=4.6.2 <4.9.0"
RUN npm install -g @compodoc/compodoc

ENV NODE_OPTIONS="--max-old-space-size=4092"

COPY . .
RUN npm run build

FROM nginx:stable-alpine
WORKDIR /var/www/html

# Remove default nginx static assets
RUN rm -rf ./*

COPY --from=builder /app/dist/projects-app-ang/ ./
COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]